package com.starnation.android.model;

import org.parceler.Parcel;

/*
 * @author lsh
 * @since 2018. 1. 19.
*/
@Parcel
public class Book {

    public Book() {
    }

    String mName;

    String mAge;

    public Book(String name, String age) {
        mName = name;
        mAge = age;
    }

    @Override
    public String toString() {
        return "mName : " + mName + " mAge : " + mAge;
    }
}
