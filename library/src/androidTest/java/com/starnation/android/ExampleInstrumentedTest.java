package com.starnation.android;

import android.content.Context;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.starnation.android.model.Book;
import com.starnation.android.os.ParcelUtil;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.starnation.android.test", appContext.getPackageName());
    }


    @Test
    public void parcelTest() throws Exception {
        Bundle bundle = new Bundle();
        ParcelUtil.putParcelable(bundle, "list", createList());
        ParcelUtil.putParcelable(bundle, "book", createBook());

        ArrayList<Book> books = ParcelUtil.getArrayList(bundle, "list");
        for (Book book : books){
            Log.d("DEBUG", book.toString());
        }

        Book book = ParcelUtil.getParcelable(bundle, "book");
        Log.d("DEBUG","item : "+book.toString());
    }

    private ArrayList<Book> createList(){

        ArrayList<Book> books = new ArrayList<>();
        books.add(new Book("1name", "1age"));
        books.add(new Book("2name", "2age"));
        return books;
    }

    private Book createBook(){
        return new Book("1name", "1age");
    }
}
