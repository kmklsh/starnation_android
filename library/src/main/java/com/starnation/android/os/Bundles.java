package com.starnation.android.os;

import android.os.Bundle;
import android.os.Parcelable;

import com.starnation.util.StringUtil;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * @author lsh
 * @since 15. 10. 5.
 */
public final class Bundles {

    //======================================================================
    // Public Methods
    //======================================================================

    public static boolean put(Bundle bundle, String key, double value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putDouble(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, String value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putString(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, boolean value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putBoolean(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, int value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putInt(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, float value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putFloat(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, long value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putLong(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, Serializable value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putSerializable(key, value);
            return true;
        }
        return false;
    }

    public static boolean putIntegerArrayList(Bundle bundle, String key, ArrayList<Integer> value) {
        if (isBundlePutUse(bundle, key) == true) {
            bundle.putIntegerArrayList(key, value);
            return true;
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, Parcelable object) {
        if (isBundlePutUse(bundle, key) == true) {
            ParcelUtil.putParcelable(bundle, key, object);
        }
        return false;
    }

    public static boolean put(Bundle bundle, String key, ArrayList<Parcelable> object) {
        if (isBundlePutUse(bundle, key) == true) {
            ParcelUtil.putParcelable(bundle, key, object);
        }
        return false;
    }

    public static ArrayList<Integer> getIntegerArrayList(Bundle bundle, String key) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getIntegerArrayList(key);
        }
        return null;
    }

    public static boolean getBoolean(Bundle bundle, String key) {
        return getBoolean(bundle, key, false);
    }

    public static boolean getBoolean(Bundle bundle, String key, boolean defaultValue) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getBoolean(key, defaultValue);
        }
        return defaultValue;
    }

    public static String getString(Bundle bundle, String key) {
        return getString(bundle, key, null);
    }

    public static String getString(Bundle bundle, String key, String defaultValue) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getString(key, defaultValue);
        }
        return defaultValue;
    }

    public static int getInt(Bundle bundle, String key) {
        return getInt(bundle, key, 0);
    }

    public static int getInt(Bundle bundle, String key, int defaultValue) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getInt(key, defaultValue);
        }
        return defaultValue;
    }

    public static float getFloat(Bundle bundle, String key) {
        return getFloat(bundle, key, 0);
    }

    public static float getFloat(Bundle bundle, String key, float defaultValue) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getFloat(key, defaultValue);
        }
        return defaultValue;
    }

    public static long getLong(Bundle bundle, String key) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getLong(key, 0);
        }
        return 0;
    }

    public static double getDouble(Bundle bundle, String key) {
        if (isBundlePutUse(bundle, key) == true) {
            return bundle.getDouble(key, 0);
        }
        return 0;
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Bundle bundle, String key) {
        if (isBundlePutUse(bundle, key) == true) {
            try {
                return (T) bundle.get(key);
            } catch (Exception e) {
                // Nothing
            }
        }
        return null;
    }

    public static ArrayList<String> getStringArrayList(Bundle bundle, String key) {
        if (isBundlePutUse(bundle, key) == true) {
            try {
                return bundle.getStringArrayList(key);
            } catch (Exception e) {
                // Nothing
            }
        }
        return null;
    }

    public static <T> T getParcelable(Bundle bundle, String key) {
        return ParcelUtil.getParcelable(bundle, key);
    }

    public static boolean isContainsKey(Bundle bundle, String key) {
        return isBundlePutUse(bundle, key) == true && bundle.containsKey(key) == true;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static boolean isBundlePutUse(Bundle bundle, String key) {
        return bundle != null && StringUtil.isEmpty(key) == false;
    }
}
