package com.starnation.android.os;

import android.os.Bundle;
import android.os.Parcelable;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 15. 5. 10.
 */
public final class ParcelUtil {

    //======================================================================
    // Variables
    //======================================================================

    @SuppressWarnings("unchecked")
    public static <T> T getParcelable(Bundle bundle, String key) {
        return getParcelableInternal(bundle, key);
    }

    public static void putParcelable(Bundle bundle, String key, Object t) {
        try {
            putParcelableInternal(bundle, key, t);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void putParcelable(Bundle bundle, String key, ArrayList list) {
        try {
            putParcelableInternal(bundle, key, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> ArrayList<T> getArrayList(Bundle bundle, String key) {
        try {
            return getParcelableInternal(bundle, key);
        } catch (Exception ignored) {
        }
        return null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    private static <T> T getParcelableInternal(Bundle bundle, String key) {
        if (bundle != null) {

            try {
                if (bundle.containsKey(key) == false) {
                    throw new NullPointerException("Not bundle value not match key");
                }
                return (T) bundle.get(key);
            } catch (Exception e) {
                // Nothing
            }
        }
        return null;
    }

    private static void putParcelableInternal(Bundle bundle, String key, Object t) {
        try {
            if (bundle != null && t != null) {
                if (t instanceof ArrayList) {
                    bundle.putParcelableArrayList(key, (ArrayList<? extends Parcelable>) t);
                } else if (t instanceof Parcelable) {
                    bundle.putParcelable(key, (Parcelable) t);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
