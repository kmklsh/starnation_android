package com.starnation.android.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import java.util.Calendar;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public final class AlarmManagerUtil {

    /**
     * 앱 재시작
     *
     * @param context
     * @param delay   지연 시간 millisecond 단위
     */
    public static void restartApplication(Context context, long delay) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        PendingIntent sender = PendingIntent.getActivity(context, 0, intent, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis() + delay, sender);
    }

    /**
     * Broadcast 을 일정시간 간격으로 호출 앱이 Slepp 모드여도 동작되게 AlarmManager.ELAPSED_REALTIME_WAKEUP 설정
     *
     * @param context
     * @param name    Broadcast ACTION_NAME
     * @param cycle   호출 간격 millisecond 단위
     */
    public static void repeatSendBroadcast(Context context, String name, long cycle) {
        Intent intent = new Intent();
        intent.setAction(name);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), cycle, sender);
    }

    /**
     * Broadcast 을 일정시간 지연시켜서 호출 Sleep 모드 일때는 동작을 안한다.
     *
     * @param context
     * @param name    Broadcast ACTION_NAME
     * @param delay   지연 시간 millisecond 단위
     */
    public static void delayedSendBroadcast(Context context, String name, long delay) {
        Intent intent = new Intent();
        intent.setAction(name);
        delayedSendBroadcast(context, intent, delay);
    }

    /**
     * Broadcast 을 일정시간 지연시켜서 호출 Slepp 모드 일때는 동작을 안한다.
     *
     * @param context
     * @param intent  Broadcast intent
     * @param delay   지연 시간 millisecond 단위
     */
    public static void delayedSendBroadcast(Context context, Intent intent, long delay) {
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis() + delay, sender);
    }
}
