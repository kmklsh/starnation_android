package com.starnation.android.app.fragment.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.starnation.android.Config;
import com.starnation.android.app.activity.internal.helper.CacheInstanceHelper;
import com.starnation.android.app.fragment.FragmentCycle;
import com.starnation.android.os.Bundles;
import com.starnation.android.util.Logger;

/*
 * @author lsh
 * @since 2016. 5. 2.
 */
public class FragmentViewModel extends ViewModel implements FragmentCycle {

    //======================================================================
    // Constants
    //======================================================================

    private final static String KEY_CACHE_INSTANCE_DATA = "KEY_CACHE_INSTANCE_DATA";

    //======================================================================
    // Variables
    //======================================================================

    @IgnoreSaveInstance
    private final int mFragmentKey;

    private SaveInstanceHelper mSaveInstanceHelper = new SaveInstanceHelper() {
        @Override
        public FragmentViewModel target() {
            return FragmentViewModel.this;
        }

        @Override
        public Class targetSuperClass() {
            return FragmentViewModel.class;
        }
    };

    //======================================================================
    // Constructor
    //======================================================================

    public FragmentViewModel(@NonNull Fragment fragment) {
        mFragmentKey = fragment.hashCode();
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mSaveInstanceHelper.onRestoredInstance(getRestoreInstance(savedInstanceState));
    }

    /**
     * {@link FragmentViewModel}는 {@link Fragment#onViewCreated(View, Bundle)} 호출을 안한다.
     *
     * @param view
     * @param savedInstanceState
     */
    @Deprecated
    @Override
    public final void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        // Nothing
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Bundle storedBundle;
        if (CacheInstanceHelper.isEnableCacheInstance() == true) {
            /*
               2017. 4. 10. lsh
               1. Bundle 다시 생성한다.
               2. 다시 생성된 Bundle을 기존 SaveInstanceHelper.onSaveInstanceState(Bundle) 에서 저장한다.
               3. Cache Storage 저장할 경로명을 생성한다.
               4. 생성한 경로에 Bundle 을 저장한다.
               4. 경로명을 outState Bundle 에 저장한다.
            */
            storedBundle = new Bundle();
            mSaveInstanceHelper.onSaveInstanceState(storedBundle);

            String cachePath = CacheInstanceHelper.makeCacheAbsolutePath(getActivity());
            CacheInstanceHelper.onSaveInstanceState(storedBundle, cachePath);
            Bundles.put(outState, KEY_CACHE_INSTANCE_DATA, cachePath);
        } else {
            mSaveInstanceHelper.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        // Nothing
    }

    @Override
    public void onDestroyView() {
        // Nothing
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        // Nothing
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Nothing
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        // Nothing
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (Config.isLogEnable()) {
            Logger.d("VIEW_MODEL", getClass().getName() + "> onCleared()");
        }
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    protected Fragment getFragment() {
        return com.starnation.android.app.fragment.Fragment.findFragment(mFragmentKey);
    }

    protected final Activity getActivity() {
        return getFragment().getActivity();
    }

    protected final Bundle getArguments() {
        return getFragment().getArguments();
    }

    protected final Resources getResources() {
        return getFragment().getResources();
    }

    protected final int getTag() {
        return getFragment().hashCode();
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private Bundle getRestoreInstance(Bundle savedInstanceState) {
        if (CacheInstanceHelper.isEnableCacheInstance() == true) {
            Bundle cacheBundle = CacheInstanceHelper.onRestoredInstance(Bundles.getString(savedInstanceState, KEY_CACHE_INSTANCE_DATA));
            if (cacheBundle != null) {
                return cacheBundle;
            }
        }
        return savedInstanceState;
    }
}
