package com.starnation.android.app.fragment.viewmodel

import android.os.Bundle
import android.os.Parcelable
import com.starnation.android.BuildConfig
import com.starnation.android.Config
import com.starnation.android.app.fragment.viewmodel.Util.firstGenericClass
import com.starnation.android.app.fragment.viewmodel.Util.getDeclaredFields
import com.starnation.android.app.fragment.viewmodel.Util.isExcludeField
import com.starnation.android.app.fragment.viewmodel.Util.isIgnoreSaveInstance
import com.starnation.android.app.fragment.viewmodel.Util.isUseParcelable
import com.starnation.android.os.Bundles
import com.starnation.android.os.ParcelUtil
import com.starnation.android.util.Logger
import com.starnation.util.StringUtil
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.lang.reflect.Field
import java.util.*

/*
 * @author lsh
 * @since 2016. 10. 20.
*/
@Suppress("UNREACHABLE_CODE")
abstract class SaveInstanceHelper {

    //======================================================================
    // Abstract Methods
    //======================================================================

    abstract fun target(): Any

    abstract fun targetSuperClass(): Class<*>?

    //======================================================================
    // Public Methods
    //======================================================================

    fun onRestoredInstance(savedInstanceState: Bundle?) {
        try {
            if (savedInstanceState == null) {
                return
            }

            var targetClass: Class<*> = target().javaClass

            while (true) {
                val fields = getDeclaredFields(targetClass)
                for (field in fields) {
                    if (isIgnoreSaveInstance(field)) {
                        continue
                    }
                    if (isExcludeField(field)) {
                        continue
                    }

                    field.isAccessible = true

                    val instanceKey = getSaveInstanceKey(field)
                    val objectType = ObjectType.findObjectTypeByFiled(field, target())

                    val result = getRestoreInstance(savedInstanceState, instanceKey, target(), field)

                    if (Config.isLogEnable() && objectType != null) {
                        Logger.w(TAG, makeLogMessage(target().javaClass.simpleName, "getRestoreInstance -> type  : $objectType key : $instanceKey value : $result"))
                    }

                    if (result == null) {
                        continue
                    }

                    field.set(target(), result)
                }

                //Modify 2016. 11. 15. lsh targetSuperClass() 지정이 안되어 있으면 loop를 종료 한다.
                if (targetSuperClass() == null) {
                    break
                } else {
                    if (targetClass.superclass == targetSuperClass() == true) {
                        break
                    } else {
                        targetClass = targetClass.superclass
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun onSaveInstanceState(outState: Bundle?) {
        try {
            if (outState == null) {
                return
            }

            var targetClass: Class<*> = target().javaClass

            while (true) {
                val fields = getDeclaredFields(targetClass)
                for (field in fields) {
                    if (isIgnoreSaveInstance(field) == true) {
                        continue
                    }
                    if (isExcludeField(field) == true) {
                        continue
                    }
                    putField(target(), outState, getSaveInstanceKey(field), field)
                }

                //Modify 2016. 11. 15. lsh targetSuperClass() 지정이 안되어 있으면 loop를 종료 한다.
                if (targetSuperClass() == null) {
                    break
                } else {
                    if (targetClass.superclass == targetSuperClass() == true) {
                        break
                    } else {
                        targetClass = targetClass.superclass
                    }
                }
            }
        } catch (e: Exception) {
            if (BuildConfig.DEBUG == true) {
                e.printStackTrace()
            }
        }
    }

    private fun getSaveInstanceKey(field: Field): String {
        return field.type.simpleName + ":" + target().javaClass.name + "." + field.name
    }

    //======================================================================
    // ObjectType
    //======================================================================

    @Suppress("UNUSED_EXPRESSION")
    internal enum class ObjectType {
        BOOLEAN,
        LONG,
        DOUBLE,
        INTEGER,
        ARRAYLIST,
        STRING,
        ENUM,
        LAZY,
        PARCEL;


        companion object {

            /**
             * Object Type 가져온다
             *
             * @param field  Field 정보
             * @param target Filed Value 를 사용하는 class
             *
             * @return [ObjectType]
             */
            fun findObjectTypeByFiled(field: Field, target: Any): ObjectType? {
                try {
                    for (excludeFieldName in EXCLUDE_FIELD_NAME_LIST) {
                        if (excludeFieldName == field.name == true) {
                            return null
                        }
                    }
                    if (isParcelInstance(field, target) == true) {
                        return PARCEL
                    }

                    val value = Util.asFieldValue(field, target)

                    if (value != null) {
                        val fileType = value::class.java.simpleName.toUpperCase()
                        for (objectType in ObjectType.values()) {
                            val o = objectType.toString()
                            if (o.contains(fileType)) {
                                return objectType
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                return null
            }

            //======================================================================
            // Private Methods
            //======================================================================

            private fun isParcelInstance(field: Field, target: Any): Boolean {
                try {
                    val fieldValue = Util.asFieldValue(field, target)

                    if (fieldValue != null && fieldValue::class.java.isAnnotationPresent(Parcelize::class.java)){
                        return true
                    }

                    if (fieldValue is Parcelable) {
                        return true
                    }

                    for (annotation in field.type.annotations) {
                        if (annotation is Parcelize) {
                            return true
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                return false
            }
        }
    }

    companion object {

        //======================================================================
        // Constants
        //======================================================================

        internal const val TAG = "FRAGMENT"

        private val EXCLUDE_FIELD_NAME_LIST = arrayOf("serialVersionUID", "\$change")

        //======================================================================
        // Private Methods
        //======================================================================

        private fun makeLogMessage(tag: String, message: String): String {
            return if (StringUtil.isEmpty(tag) == false) {
                String.format(Locale.US, "[%s]: %s", tag, message)
            } else message
        }

        private fun putField(target: Any, outState: Bundle, key: String, field: Field) {
            try {
                field.isAccessible = true
                val value: Any = Util.asFieldValue(field, target)
                        ?: throw NullPointerException("access Field(" + field.name + ") null")

                val objectType = ObjectType.findObjectTypeByFiled(field, target)
                if (objectType != null) {
                    if (Config.isLogEnable()) {
                        Logger.w(TAG, makeLogMessage(target.javaClass.simpleName, "onSaveInstanceStateInternal -> type  : $objectType key : $key"))
                    }
                    when (objectType) {
                        SaveInstanceHelper.ObjectType.BOOLEAN -> Bundles.put(outState, key, value as Boolean?)
                        SaveInstanceHelper.ObjectType.INTEGER -> Bundles.put(outState, key, value as Int?)
                        SaveInstanceHelper.ObjectType.ARRAYLIST -> {
                            putArrayList(outState, key, (value as ArrayList<*>?)!!, field)
                        }
                        SaveInstanceHelper.ObjectType.STRING -> Bundles.put(outState, key, value as String?)
                        SaveInstanceHelper.ObjectType.ENUM -> Bundles.put(outState, key, value as Serializable?)
                        SaveInstanceHelper.ObjectType.PARCEL -> ParcelUtil.putParcelable(outState, key, value)
                        SaveInstanceHelper.ObjectType.LONG -> {
                            Bundles.put(outState, key, value as Long?)
                        }
                        SaveInstanceHelper.ObjectType.DOUBLE -> {
                            Bundles.put(outState, key, value as Double)
                        }
                        else -> {
                        }
                    }
                }
            } catch (e: Exception) {
                if (Config.isLogEnable()) {
                    Logger.e(TAG, makeLogMessage(target.javaClass.getSimpleName(), "$e"))
                }
            }
        }

        private fun getRestoreInstance(savedInstanceState: Bundle, key: String, target: Any, field: Field): Any? {
            val objectType = ObjectType.findObjectTypeByFiled(field, target)
            if (objectType != null) {
                val value = when (objectType) {
                    SaveInstanceHelper.ObjectType.ARRAYLIST -> getArrayList(savedInstanceState, key, field, target)
                    SaveInstanceHelper.ObjectType.BOOLEAN -> Bundles.getBoolean(savedInstanceState, key)
                    SaveInstanceHelper.ObjectType.INTEGER -> Bundles.getInt(savedInstanceState, key)
                    SaveInstanceHelper.ObjectType.STRING -> Bundles.getString(savedInstanceState, key)
                    SaveInstanceHelper.ObjectType.PARCEL -> Util.getParcelable<Any>(savedInstanceState, key)
                    SaveInstanceHelper.ObjectType.ENUM -> Bundles.get<Any>(savedInstanceState, key)
                    SaveInstanceHelper.ObjectType.LONG -> Bundles.getLong(savedInstanceState, key)
                    SaveInstanceHelper.ObjectType.DOUBLE -> Bundles.getDouble(savedInstanceState, key)
                    else -> {
                    }
                }
                //Modify 2018. 3. 14. lsh lazy 선언된경우 예외처리
                return if (key.startsWith("Lazy") == true) {
                    lazyOf(value)
                } else {
                    value
                }
            }
            return null
        }

        private fun putArrayList(outState: Bundle, key: String, value: ArrayList<*>, field: Field) {
            try {
                val clazz = firstGenericClass(field, value)
                if (clazz != null) {
                    for (annotation in clazz.annotations) {
                        if (annotation is Parcelize) {
                            ParcelUtil.putParcelable(outState, key, value)
                            return
                        }
                    }

                    if (clazz == Int::class.java == true) {
                        Bundles.put(outState, key, value)
                    } else if (clazz == String::class.java == true) {
                        Bundles.put(outState, key, value)
                    } else {
                        if (isUseParcelable(value) == true) {
                            ParcelUtil.putParcelable(outState, key, value)
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun getArrayList(savedInstanceState: Bundle, key: String, field: Field, fieldTarget: Any): ArrayList<*>? {
            val fieldValue: Any
            try {
                fieldValue = Util.asFieldValue(field, fieldTarget)!!

                val clazz = firstGenericClass(field, fieldValue as ArrayList<*>)
                if (clazz != null) {
                    for (annotation in clazz.annotations) {
                        if (annotation is Parcelize) {
                            return Util.getArrayList<Any>(savedInstanceState, key)
                        }
                    }

                    if (clazz == Int::class.java) {
                        return Bundles.getIntegerArrayList(savedInstanceState, key)
                    } else if (clazz == String::class.java) {
                        return Bundles.getStringArrayList(savedInstanceState, key)
                    } else {
                        if (isUseParcelable(fieldValue) == true) {
                            return Util.getArrayList<Any>(savedInstanceState, key)
                        }
                    }
                }
                return fieldValue
            } catch (e: Exception) {
                //Nothing
            }
            return null
        }
    }
}
