package com.starnation.android.app.fragment;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.starnation.android.app.fragment.viewmodel.FragmentViewModel;
import com.starnation.eventbus.Event;
import com.starnation.eventbus.OnEventBusListener;

/*
 * @author lsh
 * @since 2016. 5. 2.
 */
public interface FragmentSupport<P extends FragmentViewModel, E extends Event> extends OnEventBusListener<E> {

    /**
     * {@link #getViewmodel()} 사용 권장
     * @return
     * @see #getViewmodel()
     */
    @Deprecated
    P getPresenter();

    P getViewmodel();

    /**
     * {@link android.support.v4.app.Fragment} 화면 이동 여부
     *
     * @return true 면 이전 {@link android.support.v4.app.Fragment} 이동
     *
     * @see Toolbar#setNavigationOnClickListener(View.OnClickListener)
     * @see AppCompatActivity#onBackPressed()
     */
    boolean onBackPressed();

    /**
     * @return {@link android.support.v4.app.Fragment#isAdded()} or {@link android.support.v4.app.Fragment#getUserVisibleHint()} true 여부 반환
     */
    boolean isFragmentActivated();

    void popBackStackImmediate();

    FragmentEventBus eventBus();
}
