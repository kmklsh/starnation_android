package com.starnation.android.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import com.starnation.android.app.fragment.viewmodel.FragmentViewModel;

import static com.starnation.android.app.fragment.DialogFragment.OnDismissCallback;

/*
 * @author lsh
 * @since 2016. 4. 18.
*/
abstract class DialogFragmentDelegate<P extends FragmentViewModel> extends FragmentDelegate<P> {

    //======================================================================
    // Variables
    //======================================================================

    OnDismissCallback mDismissCallback;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDismissCallback = null;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void dismiss(@NonNull Bundle result) {
        Fragment fragment = getFragment();
        if (fragment != null && fragment instanceof DialogFragment) {
            ((DialogFragment) fragment).dismiss();
        }
        if (mDismissCallback != null) {
            mDismissCallback.onDismiss(result);
        }
    }

    public void setDismissCallback(OnDismissCallback dismissCallback) {
        mDismissCallback = dismissCallback;
    }
}
