package com.starnation.android.app.fragment;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.starnation.android.Config;
import com.starnation.android.app.fragment.viewmodel.FragmentViewModel;
import com.starnation.android.util.Logger;
import com.starnation.eventbus.EventBus;
import com.starnation.util.ClassUtil;
import com.starnation.util.StringUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

/*
 * @author lsh
 * @since 2016. 4. 18.
 */
abstract class FragmentDelegate<P extends FragmentViewModel> implements FragmentCycle {

    //======================================================================
    // Constants
    //======================================================================

    private final static String TAG = "FRAGMENT";

    //======================================================================
    // Variables
    //======================================================================

    private P mViewModel;

    private final static FragmentList sFragmentList = new FragmentList();

    private FragmentSupport mFragmentSupport;

    //======================================================================
    // Abstract Methods
    //======================================================================

    protected abstract Fragment getFragment();

    //======================================================================
    // Override Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Fragment fragment = getFragment();
        mFragmentSupport = (FragmentSupport) fragment;
        sFragmentList.onCreate(fragment);
        EventBus.with().register(fragment);
        bindViewModel();
        if (mViewModel != null) {
            mViewModel.onCreate(savedInstanceState);
        }
        cycle("onCreate -> savedInstanceState : " + savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        cycle("onViewCreated -> savedInstanceState : " + savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        cycle("onSaveInstanceState -> outState : " + outState);
        if (mViewModel != null) {
            mViewModel.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        cycle("onViewStateRestored -> savedInstanceState : " + savedInstanceState);
        if (mViewModel != null) {
            mViewModel.onViewStateRestored(savedInstanceState);
        }
    }

    @Override
    public void setUserVisibleHint(boolean setUserVisibleHint) {
        cycle("setUserVisibleHint -> setUserVisibleHint : " + setUserVisibleHint);
        if (mViewModel != null) {
            mViewModel.setUserVisibleHint(setUserVisibleHint);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mViewModel != null) {
            mViewModel.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (mViewModel != null) {
            mViewModel.onPrepareOptionsMenu(menu);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        cycle("onActivityResult -> requestCode : " + requestCode + " resultCode : " + resultCode + " data : " + data);
        if (mViewModel != null) {
            mViewModel.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroyView() {
        cycle("onDestroyView");
    }

    @SuppressWarnings("EmptyCatchBlock")
    @Override
    public void onDestroy() {
        cycle("onDestroy");
        Fragment fragment = getFragment();
        EventBus.with().unRegister(fragment);
        sFragmentList.onDestroy(fragment);

        if (mViewModel != null) {
            mViewModel.onDestroy();
            mViewModel = null;
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static FragmentViewModel findViewModel(final Fragment fragment, Class<? extends FragmentViewModel> aClass) {
        return ViewModelProviders.of(fragment, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                try {
                    return modelClass.getDeclaredConstructor(Fragment.class).newInstance(fragment);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).get(aClass);
    }

    /**
     * {@link FragmentList#findFragment(int)}
     */
    public static Fragment findFragment(int key) {
        return sFragmentList.findFragment(key);
    }

    public boolean isFragmentActivated() {
        Fragment fragment = getFragment();
        return fragment.isAdded() && fragment.getUserVisibleHint();
    }

    public P getViewModel() {
        return mViewModel;
    }

    public void popBackStackImmediate() {
        boolean backPressed = mFragmentSupport.onBackPressed();
        Fragment fragment = getFragment();
        if (backPressed == true) {
            if (fragment.getFragmentManager().getBackStackEntryCount() > 0) {
                fragment.getFragmentManager().popBackStackImmediate();
            } else {
                ActivityCompat.finishAfterTransition(fragment.getActivity());
            }
        }
    }

    public FragmentEventBus eventBus() {
        Fragment fragment = getFragment();
        return FragmentEventBus.with(fragment);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static String makeLogMessage(String tag, String message) {
        if (StringUtil.isEmpty(tag) == false) {
            return String.format(Locale.US, "[%s]: %s", tag, message);
        }
        return message;
    }

    @SuppressWarnings("unchecked")
    private void bindViewModel() {
        try {
            Fragment fragment = getFragment();
            Class<P> innerClass = (Class<P>) ClassUtil.getReclusiveGenericClass(fragment.getClass(), 0);
            mViewModel = (P) findViewModel(fragment, innerClass);
        } catch (Exception e) {
            if (Config.isLogEnable()) {
                Logger.e(TAG, "Presenter Create Fail -> e : " + e.getMessage());
            }
        }
    }

    private void cycle(String message) {
        Fragment fragment = getFragment();
        if (fragment == null) {
            return;
        }
        String objectName = fragment.getClass().getSimpleName();
        if (Config.isLogEnable()) {
            Logger.i(TAG, makeLogMessage(objectName, message));
        }
    }
}
