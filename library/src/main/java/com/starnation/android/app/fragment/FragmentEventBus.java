package com.starnation.android.app.fragment;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.starnation.eventbus.Event;
import com.starnation.eventbus.EventBus;

/*
 * @author lsh
 * @since 2017. 4. 7.
*/
public final class FragmentEventBus {

    //======================================================================
    // Variables
    //======================================================================

    Fragment mFragment;

    Target mTarget = Target.SELF;

    //======================================================================
    // Constructor
    //======================================================================

    public static FragmentEventBus with(Fragment fragment) {
        FragmentEventBus bus = new FragmentEventBus();
        bus.mFragment = fragment;
        return bus;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public FragmentEventBus target(Target target) {
        mTarget = target;
        return this;
    }

    @SuppressWarnings("unchecked")
    public void post(@NonNull Event event) {
        switch(mTarget) {
            case PARENT:
                EventBus.with().post(mFragment.getParentFragment(), event);
                break;
            case SELF:
                EventBus.with().post(mFragment, event);
                break;
            case ALL:
                EventBus.with().postAll(event);
                break;
        }
    }

    //======================================================================
    // EventTarget
    //======================================================================

    public enum Target {
        PARENT,
        SELF,
        ALL,
    }
}
