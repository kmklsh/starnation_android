package com.starnation.android.app;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
@Deprecated
public final class KeyguardManagerUtil {

    /**
     * Android 단말 잠금 모드
     *
     * @param context
     */
    public static void screenLock(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(Activity.KEYGUARD_SERVICE);
        lock.disableKeyguard();
    }

    /**
     * Android 단말 잠금 모드 해제
     *
     * @param context
     */
    public static void screenULock(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(Activity.KEYGUARD_SERVICE);
        lock.reenableKeyguard();
    }

    /**
     * Android 단말 잠금 모드 인지 확인
     *
     * @param context
     *
     * @return 잠금 모드 인경우 true 리턴
     */
    public static boolean isScreenLock(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        return keyguardManager.inKeyguardRestrictedInputMode();
    }
}
