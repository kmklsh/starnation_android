package com.starnation.android.app.fragment.viewmodel

import android.os.Bundle
import android.os.Parcelable
import com.starnation.android.util.Logger
import com.starnation.util.FieldUtil
import kotlinx.android.parcel.Parcelize
import java.lang.reflect.Field
import java.lang.reflect.Modifier

/*
 * @author lsh
 * @since 2018. 1. 19.
*/
internal object Util {

    fun <T> getParcelable(bundle: Bundle?, key: String): T? {
        if (bundle == null) {
            return null
        }
        try {
            if (bundle.containsKey(key) == false) {
                throw Exception("Not bundle value not match key")
            }
            return bundle.get(key) as T?
        } catch (e: Exception) {
            // Nothing
        }
        return null
    }

    fun <T> getArrayList(bundle: Bundle?, key: String): ArrayList<Parcelable>? {
        try {
            if (bundle != null) {
                try {
                    val list: ArrayList<Parcelable> = bundle.getParcelableArrayList(key)
                    return list
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun asFieldValue(field: Field, target: Any): Any? {
        if (field.isAccessible == false) {
            field.isAccessible = false
        }

        return field.get(target).also {
            return when (it) {
                is Lazy<*> -> {
                    it.value
                }
                else -> {
                    it
                }
            }
        }
    }

    fun firstGenericClass(field: Field, value: ArrayList<*>): Class<*>? {
        var actualType: Class<*>? = null
        try {
            actualType = (FieldUtil.getActualTypeArguments(field, 0) as Class<*>)
        } catch (e: Exception) {
            actualType = null
            Logger.w(SaveInstanceHelper.TAG, "firstGenericClass error : $e")
        }

        try {
            if (actualType == null) {
                actualType = value::class.java.typeParameters[0]::class.java
            }
        } catch (e: Exception) {
            Logger.w(SaveInstanceHelper.TAG, "firstGenericClass error : $e")
        }
        return actualType
    }

    fun isUseParcelable(value: ArrayList<*>): Boolean {
        var matchCount = 0;
        if (value.isNotEmpty() == true) {
            value.forEach { item ->
                for (annotation in item::class.java.annotations) {
                    if (annotation is Parcelize) {
                        matchCount++
                        break
                    }
                }

                if (item is Parcelable) {
                    matchCount++
                    return@forEach
                }
            }
            return matchCount == value.size
        }
        return false
    }

    fun isIgnoreSaveInstance(field: Field): Boolean {
        val ignoreSaveInstance = field.getAnnotation<IgnoreSaveInstance>(IgnoreSaveInstance::class.java)
        return ignoreSaveInstance != null
    }

    fun isExcludeField(field: Field): Boolean {
        if (Modifier.isStatic(field.modifiers) && Modifier.isFinal(field.modifiers)) {
            return true
        }
        return false
    }

    fun getDeclaredFields(targetClass: Class<*>): Array<Field> {
        return targetClass.declaredFields
    }
}
