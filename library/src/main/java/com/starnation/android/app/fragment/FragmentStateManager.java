package com.starnation.android.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * @author lsh
 * @since 15. 01. 16.
*/
public final class FragmentStateManager {

    //=========================================================================
    // Constants
    //=========================================================================

    public final static int FLAG_SINGLE_TOP = 1;

    private final static int NO_FLAG = -1;

    private final static int MIN_FRAGMENT_COUNT = 1;

    private final static String KEY_SAVE_FRAGMENT_STACK_LIST = "SaveFragmentStackList";

    //=========================================================================
    // Variables
    //=========================================================================

    private FragmentManager mManager;

    private Bundle mCurrentResultBundle;

    private Activity mActivity;

    private Fragment mCurrentFragment;

    @IdRes
    private int mContainerId;

    StackImpl mStack = new StateStack();

    //=========================================================================
    // Constructor
    //=========================================================================

    public static FragmentStateManager create(@IdRes int containerId, Activity activity, FragmentManager fragmentManager) {
        FragmentStateManager manager = new FragmentStateManager(fragmentManager);
        manager.mContainerId = containerId;
        manager.mActivity = activity;
        return manager;
    }

    public FragmentStateManager(FragmentManager manager) {
        mManager = manager;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Transaction beginTransaction() {
        return new Transaction();
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public boolean popBackStack() {
        Fragment fragment = mCurrentFragment;
        if (fragment != null && fragment instanceof OnFragmentResultBundleListener) {
            Bundle bundle = ((OnFragmentResultBundleListener) fragment).onCreateResultBundle();
            return popBackStack(bundle);
        }
        return popBackStack(null);
    }

    public boolean popBackStack(Bundle bundle) {
        return mStack.popBackStack(bundle);
    }

    public void saveState(Bundle bundle) {
        mStack.savedState(bundle);
    }

    public void restoreState(Bundle bundle) {
        mStack.restoreState(bundle);
    }

    public void onStart() {
        Fragment fragment = mCurrentFragment;
        if (mCurrentResultBundle != null && fragment != null && fragment instanceof OnFragmentResultListener) {
            OnFragmentResultListener onFragmentResultListener = (OnFragmentResultListener) fragment;
            onFragmentResultListener.onFragmentResult(mCurrentResultBundle);
            mCurrentResultBundle = null;
        }
    }

    public void release() {
        mCurrentResultBundle = null;
        mActivity = null;
    }

    public void clearFragmentStack() {
        mStack.release();
        mCurrentFragment = null;
    }

    //=========================================================================
    // Private Method
    //=========================================================================

    private static Fragment createFragment(Activity activity, FragmentItem fragmentItem) {
        Bundle bundle = fragmentItem.bundle != null ? fragmentItem.bundle : new Bundle();
        return Fragment.instantiate(activity, fragmentItem.className, bundle);
    }

    @SuppressWarnings("ResourceType")
    private void popShowFragment(Activity activity, FragmentItem fragmentItem, Bundle bundle) {
        try {
            if (fragmentItem == null) {
                return;
            }
            Fragment fragment = createFragment(activity, fragmentItem);
            fragment.setInitialSavedState(fragmentItem.savedState);

            //Modify 15. 8. 21. lsh FLAG 에 따라 기존 Fragment 가 삭제 될수 있다.. FLAG 처리 완료후 ID를 지정 해야된다
            beginTransaction().replace(fragment).commit();

            if (fragment instanceof OnFragmentResultListener) {
                mCurrentResultBundle = bundle;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //======================================================================
    // OnFragmentResultListener
    //======================================================================

    public interface OnFragmentResultListener {
        void onFragmentResult(Bundle bundle);
    }

    //======================================================================
    // OnFragmentResultBundleListener
    //======================================================================

    public interface OnFragmentResultBundleListener {
        Bundle onCreateResultBundle();
    }

    //======================================================================
    // BackPressed
    //======================================================================

    public interface BackPressed {
        /**
         * {@link Fragment} Level 에서 {@link Activity#onBackPressed()} 호출시 제어
         *
         * @return true 면 {@link Activity#onBackPressed()} false 면 호출하지 않음
         */
        boolean onBackPressed();
    }

    //======================================================================
    // FragmentTransaction
    //======================================================================

    public class Transaction {

        int mFragmentFlag = NO_FLAG;

        boolean mAddBackStack;

        Fragment mStayFragment;

        public Transaction() {
        }

        public Transaction setFlag(int fragmentFlag) {
            mFragmentFlag = fragmentFlag;
            return this;
        }

        public Transaction addSharedElement(View sharedElement, String name) {
            beginTransaction().addSharedElement(sharedElement, name);
            return this;
        }

        public Transaction addToBackStack() {
            mAddBackStack = true;
            return this;
        }

        public Transaction replace(Fragment fragment) {
            mStayFragment = fragment;
            return this;
        }

        public int commit() {
            if (mStayFragment == null) {
                return -1;
            }

            mStack.savedState();

            Fragment currentFragment = findFlagFragment(mFragmentFlag, mStayFragment);
            if (mAddBackStack == true) {
                mStack.add(currentFragment);
            }
            mCurrentFragment = currentFragment;
            mStayFragment = null;
            return beginTransaction().replace(mContainerId, currentFragment).commit();
        }

        /**
         * fragmentFlag 일치되는 {@link Fragment} 반환
         *
         * @param frag     fragmentFlag
         * @param fragment {@link Fragment}
         *
         * @return {@link Fragment} 없으면 파라미터로 받은 {@link Fragment} 반환
         *
         * @see #mFragmentFlag
         */
        Fragment findFlagFragment(int frag, Fragment fragment) {
            return mStack.findFlagFragment(frag, fragment);
        }

        FragmentTransaction beginTransaction() {
            return mManager.beginTransaction();
        }
    }

    //======================================================================
    // StackImpl
    //======================================================================

    interface StackImpl {
        boolean popBackStack();

        boolean popBackStack(Bundle bundle);

        void add(Fragment currentFragment);

        Fragment findFlagFragment(int frag, Fragment fragment);

        void savedState();

        void savedState(Bundle bundle);

        void restoreState(Bundle bundle);

        void release();
    }

    //======================================================================
    // StateStack
    //======================================================================

    final class StateStack implements StackImpl {

        ArrayList<FragmentItem> mFragmentStack = new ArrayList<>();

        @Override
        public boolean popBackStack() {
            return popBackStack(null);
        }

        @Override
        public boolean popBackStack(Bundle bundle) {
            try {
                int size = mFragmentStack.size();
                if (size > MIN_FRAGMENT_COUNT) {
                    Fragment fragment = mCurrentFragment;
                    if (fragment != null && fragment instanceof BackPressed) {
                        boolean backPressed = ((BackPressed) fragment).onBackPressed();
                        //Modify 2015. 11. 11. lsh Fragment level 에서 return 값이 false processPopStackFragment 호출하지 않는다.
                        if (backPressed == false) {
                            return true;
                        }
                    }
                    if (size > 0) {
                        mFragmentStack.remove(size - 1);
                    }

                    size = mFragmentStack.size();
                    if (size > 0) {
                        FragmentItem fragmentItem = mFragmentStack.get(size - 1);
                        if (fragmentItem != null) {
                            popShowFragment(mActivity, fragmentItem, bundle);
                        }
                    }
                    return true;
                }
                return false;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        public void savedState() {
            if (mFragmentStack.size() > 0 && mCurrentFragment != null) {
                //Modify 15. 9. 7. lsh Fragment 이름이 똑같은지 확인을 해봐야 한다.
                FragmentItem item = mFragmentStack.get(mFragmentStack.size() - 1);
                if (StringUtil.equals(item.className, mCurrentFragment.getClass().getName()) == true) {
                    item.savedState = mManager.saveFragmentInstanceState(mCurrentFragment);
                }
            }
        }

        @Override
        public void savedState(Bundle bundle) {
            if (bundle != null) {
                bundle.putParcelableArrayList(KEY_SAVE_FRAGMENT_STACK_LIST, mFragmentStack);
            }
        }

        @Override
        public void restoreState(Bundle bundle) {
            if (bundle != null) {
                ArrayList<FragmentItem> list = bundle.getParcelableArrayList(KEY_SAVE_FRAGMENT_STACK_LIST);
                if (CollectionValidator.isValid(list)) {
                    mFragmentStack.addAll(list);
                }
            }
        }

        @Override
        public void release() {
            mFragmentStack.clear();
        }

        @Override
        public void add(Fragment currentFragment) {
            int id = mFragmentStack.size();
            String className = ((Object) currentFragment).getClass().getName();
            FragmentItem addFragmentItem = new FragmentItem(
                    className,
                    id,
                    mContainerId,
                    currentFragment.getArguments());
            mFragmentStack.add(addFragmentItem);
        }

        @Override
        public Fragment findFlagFragment(int frag, Fragment fragment) {
            Fragment resultFragment = null;
            if (frag == FLAG_SINGLE_TOP) {
                Iterator<FragmentItem> iterator = mFragmentStack.iterator();
                boolean removeFragment = false;
                while (iterator.hasNext()) {
                    FragmentItem fragmentItem = iterator.next();
                    if (fragmentItem != null) {
                        String className = ((Object) fragment).getClass().getName();
                        if (StringUtil.equals(className, fragmentItem.className) == true && removeFragment == false) {
                            resultFragment = Fragment.instantiate(mActivity, className, fragment.getArguments());
                            if (fragmentItem.savedState != null) {
                                resultFragment.setInitialSavedState(fragmentItem.savedState);
                            }
                            removeFragment = true;
                        }
                    }
                    if (removeFragment == true) {
                        iterator.remove();
                    }
                }
            }
            if (resultFragment == null) {
                resultFragment = fragment;
            }
            return resultFragment;
        }
    }

    //=========================================================================
    // FragmentItem
    //=========================================================================

    final static class FragmentItem implements Parcelable {

        String className;

        int id;

        Bundle bundle;

        Fragment.SavedState savedState;

        @IdRes
        int containerId;

        public FragmentItem(String className, int id, int containerId, Bundle bundle) {
            this.className = className;
            this.id = id;
            this.containerId = containerId;
            this.bundle = bundle;
        }

        public FragmentItem(Parcel parcel) {
            className = parcel.readString();
            id = parcel.readInt();
            containerId = parcel.readInt();
            bundle = parcel.readBundle();
            savedState = parcel.readParcelable(Fragment.SavedState.class.getClassLoader());
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int flags) {
            parcel.writeString(className);
            parcel.writeInt(id);
            parcel.writeInt(containerId);
            parcel.writeBundle(bundle);
            parcel.writeParcelable(savedState, 0);
        }

        public static final Creator<FragmentItem> CREATOR = new Creator<FragmentItem>() {

            @Override
            public FragmentItem createFromParcel(Parcel source) {
                return new FragmentItem(source);
            }

            @Override
            public FragmentItem[] newArray(int size) {
                return new FragmentItem[size];
            }
        };
    }
}
