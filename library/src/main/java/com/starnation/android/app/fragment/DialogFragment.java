package com.starnation.android.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.starnation.android.app.fragment.viewmodel.FragmentViewModel;
import com.starnation.eventbus.Event;

import org.greenrobot.eventbus.Subscribe;

/*
 * @author lsh
 * @since 2016. 5. 8.
*/
public abstract class DialogFragment<P extends FragmentViewModel, E extends Event> extends android.support.v7.app.AppCompatDialogFragment implements DialogFragmentSupport<P, E> {

    //======================================================================
    // Variables
    //======================================================================

    private final DialogFragmentDelegate<P> mDelegate = new DialogFragmentDelegate<P>() {
        @Override
        protected Fragment getFragment() {
            return DialogFragment.this;
        }
    };

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDelegate.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mDelegate.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDelegate.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        mDelegate.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mDelegate.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //Modify 2016. 4. 18. lsh Fragment.onCreate() 보다 먼저 호출되는 경우 있음
        mDelegate.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDelegate.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mDelegate.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mDelegate.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDelegate.onDestroy();
    }

    @Deprecated
    @Override
    public final P getPresenter() {
        return mDelegate.getViewModel();
    }

    @SuppressWarnings("unchecked")
    @Override
    public final P getViewmodel() {
        return mDelegate.getViewModel();
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
    
    @Override
    public final boolean isFragmentActivated() {
        return mDelegate.isFragmentActivated();
    }

    /*@Override
    public AppCompatActivityDelegate getActivityDelegate() {
        return mDelegate.getActivityDelegate();
    }*/

    @Override
    public FragmentEventBus eventBus() {
        return mDelegate.eventBus();
    }

    @Override
    public void popBackStackImmediate() {
        mDelegate.popBackStackImmediate();
    }

    @Override
    public void dismiss(@NonNull Bundle result) {
        mDelegate.dismiss(result);
    }

    @Override
    public void setDismissCallback(DialogFragment.OnDismissCallback dismissCallback) {
        mDelegate.setDismissCallback(dismissCallback);
    }

    @Subscribe
    @Override
    public void onEvent(E event) {
        // Nothing
    }

    //======================================================================
    // OnDismissCallback
    //======================================================================

    public interface OnDismissCallback {
        void onDismiss(@NonNull Bundle event);
    }
}
