package com.starnation.android.app;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import java.util.List;

/**
 * Application 동작
 *
 * @author lsh
 * @since 14. 8. 20.
 */
public final class ApplicationBehavior {

    /**
     * Broadcast 동작중인지 확인
     *
     * @param context
     * @param name    Broadcast ACTION_NAME
     *
     * @return Broadcast 동작중이면 true 리턴 한다.
     */
    public static boolean isBroadcastActivated(Context context, String name) {
        Intent intent = new Intent();
        intent.setAction(name);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
        return sender != null;
    }


    /**
     * classNmae 으로 해상 Service 가 살아 있는지 확인
     *
     * @param context
     * @param name Service ClassName
     *
     * @return Service 가 살아 있으면 true
     */
    public static boolean isServiceAlive(Context context, String name) {
        boolean result = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> services = am.getRunningServices(100);
        for (ActivityManager.RunningServiceInfo info : services) {
            if (info.service.getClassName().equalsIgnoreCase(name)) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Application 실행여부 판단
     *
     * @param context
     * @param packageName packageName
     *
     * @return packageName 으로 앱 실행여부 판단
     */
    public static boolean isRunningProcess(Context context, String packageName) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> apps = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : apps) {
            if (info.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Activity Stack 에 가장 Top 올라온 RunningTaskInfo 정보를 가져온다.
     *
     * @param context
     *
     * @return RunningTaskInfo
     */
    public static ActivityManager.RunningTaskInfo getTopRunningTaskInfo(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1000);
        for (ActivityManager.RunningTaskInfo runningTaskInfo : taskInfo) {
            if (runningTaskInfo.topActivity.getPackageName().compareToIgnoreCase(context.getPackageName()) == 0) {
                return runningTaskInfo;
            }
        }
        return null;
    }

    public static int getStatusBarHeight(@NonNull Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
