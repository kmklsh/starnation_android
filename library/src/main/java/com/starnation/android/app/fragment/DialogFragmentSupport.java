package com.starnation.android.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.starnation.android.app.fragment.viewmodel.FragmentViewModel;
import com.starnation.eventbus.Event;

/*
 * @author lsh
 * @since 2017. 4. 6.
*/
public interface DialogFragmentSupport<P extends FragmentViewModel, E extends Event> extends FragmentSupport<P, E> {
    /**
     * {@link DialogFragment} 종료시 {@link Bundle} 전달
     *
     * @param result {@link Bundle}
     *
     * @see {@link DialogFragment#dismiss()}
     */
    void dismiss(@NonNull Bundle result);

    void setDismissCallback(com.starnation.android.app.fragment.DialogFragment.OnDismissCallback dismissCallback);
}
