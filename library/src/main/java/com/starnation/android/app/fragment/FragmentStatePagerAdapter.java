/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.starnation.android.app.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;
import java.util.Locale;

/*
 * @author lsh
 * @since 15. 2. 5.
*/
public abstract class FragmentStatePagerAdapter extends PagerAdapter {

    //======================================================================
    // Constants
    //======================================================================

    /**
     * {@link Fragment} tag 저장 하는 format
     */
    private static final String FORMAT_FRAGMENT_TAG = "%s.key.fragment.%d";

    private static final String KEY_SAVE_INSTANCE = "KEY_SAVE_INSTANCE";

    private static final String TAG = "PagerAdapter";

    private static final boolean DEBUG = false;

    //======================================================================
    // Variables
    //======================================================================

    private ArrayList<FragmentItem> mFragmentItems = new ArrayList<>();

    private final FragmentManager mFragmentManager;

    private ArrayList<Fragment.SavedState> mSavedState = new ArrayList<>();

    private ArrayList<Fragment> mFragments = new ArrayList<>();

    private FragmentTransaction mCurTransaction = null;

    private Fragment mCurrentPrimaryItem = null;

    //======================================================================
    // Constructor
    //======================================================================

    public FragmentStatePagerAdapter(FragmentManager fm) {
        mFragmentManager = fm;

        //Modify 15. 10. 8. lsh
        //FragmentManager Source Code 를 보면 Fragment 를 저장 하는게 아니다.
        //Fragment State 저장 하고 다시 불러올때 Fragment 를 생성 하고 상태값을 init 하고 있다.
        if (CollectionValidator.isValid(mFragmentManager.getFragments()) == true) {
            mFragments.clear();
            mFragments.addAll(mFragmentManager.getFragments());
        }
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract Context getContext();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public CharSequence getPageTitle(int position) {
        return getItem(position).mTitle;
    }

    @Override
    public int getCount() {
        return mFragmentItems.size();
    }

    @Override
    public void startUpdate(ViewGroup container) {
    }

    @SuppressLint("CommitTransaction")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment f = findFragment(position);
        if (f != null) {
            return f;
        }

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }

        Fragment fragment = getItem(position).mFragment;

        if (DEBUG) Log.v(TAG, "Adding item #" + position + ": f=" + fragment);
        if (mSavedState.size() > position) {
            Fragment.SavedState fss = mSavedState.get(position);
            if (fss != null) {
                fragment.setInitialSavedState(fss);
            }
        }
        while (mFragments.size() <= position) {
            mFragments.add(null);
        }
        fragment.setMenuVisibility(false);
        fragment.setUserVisibleHint(false);
        mFragments.set(position, fragment);

        //Modify 15. 10. 15. lsh 완료 되면 tag 저장
        mCurTransaction.add(container.getId(), fragment, getTagFormat(fragment, position));
        return fragment;
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        Fragment fragment = (Fragment) object;

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        if (DEBUG) {
            Log.v(TAG, "Removing item #" + position + ": f=" + object + " v=" + ((Fragment) object).getView());
        }
        while (mSavedState.size() <= position) {
            mSavedState.add(null);
        }
        mSavedState.set(position, mFragmentManager.saveFragmentInstanceState(fragment));
        mFragments.set(position, null);

        mCurTransaction.remove(fragment);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (DEBUG) {
            Log.d(TAG, "setPrimaryItem -> position : " + position + " object : " + object);
        }
        Fragment fragment = (Fragment) object;
        if (fragment != mCurrentPrimaryItem) {
            if (mCurrentPrimaryItem != null) {
                mCurrentPrimaryItem.setMenuVisibility(false);
                mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            mCurrentPrimaryItem = fragment;
        }
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        if (mCurTransaction != null) {
            mCurTransaction.commitAllowingStateLoss();
            mCurTransaction = null;
            mFragmentManager.executePendingTransactions();
        }
        if (DEBUG) {
            Log.d(TAG, "finishUpdate - > fragmentCount : " + mFragmentManager.getFragments().size());
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return ((Fragment) object).getView() == view;
    }

    @Override
    public Parcelable saveState() {
        if (DEBUG) {
            Log.d(TAG, "saveState");
        }
        Bundle state = null;

        if (CollectionValidator.isValid(mFragments) == true) {
            for (int i = 0; i < mFragments.size(); i++) {
                Fragment fragment = mFragments.get(i);
                if (fragment != null) {
                    if (CollectionValidator.isValid(mSavedState, i) == false) {
                        while (mSavedState.size() <= i) {
                            mSavedState.add(null);
                        }
                    }

                    try {
                        //Modify 2015. 11. 30. lsh crash 발생 -> 이미 상태값이 저장된 상태에서 저장 처리 하면 에러가 난다.
                        mSavedState.set(i, mFragmentManager.saveFragmentInstanceState(mFragments.get(i)));
                    } catch (Exception e) {
                        if (DEBUG) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (mSavedState.size() > 0) {
            state = new Bundle();
            Fragment.SavedState[] fss = new Fragment.SavedState[mSavedState.size()];
            mSavedState.toArray(fss);
            state.putParcelableArray(KEY_SAVE_INSTANCE, fss);
        }
        return state;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        if (DEBUG) {
            Log.d(TAG, "restoreState -> state : " + state + " loader : " + loader + " fragmentManager child fragment size : " + CollectionValidator.getSize(mFragmentManager.getFragments()));
        }

        if (state != null) {
            Bundle bundle = (Bundle) state;
            bundle.setClassLoader(loader);
            Parcelable[] fss = bundle.getParcelableArray(KEY_SAVE_INSTANCE);
            mSavedState.clear();
            if (fss != null) {
                for (Parcelable fs : fss) {
                    mSavedState.add((Fragment.SavedState) fs);
                }
            }
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public FragmentItem getItem(int position) {
        if (position < mFragmentItems.size()) {
            return mFragmentItems.get(position);
        }
        return null;
    }

    public Fragment getFragment(int position) {
        if (position < mFragments.size()) {
            return mFragments.get(position);
        }
        return null;
    }

    public void add(String title, Fragment fragment) {
        mFragmentItems.add(new FragmentItem(title, fragment));
    }

    public void remove(int position) {
        if (CollectionValidator.isValid(mFragmentItems, position) == true) {
            mFragmentItems.remove(position);
        }
    }

    public void clear() {
        mFragmentItems.clear();
    }

    public Fragment getCurrentPrimaryItem() {
        return mCurrentPrimaryItem;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    /**
     * {@link FragmentTransaction#add(Fragment fragment, String tag)} 저장 될 tag 를 만든다.
     *
     * @param fragment {@link Fragment}
     * @param position {@link #mFragments} 위치
     *
     * @return tag
     */
    private static String getTagFormat(@NonNull Fragment fragment, int position) {
        return String.format(Locale.US, FORMAT_FRAGMENT_TAG, fragment.getClass().getSimpleName(), position);
    }

    /**
     * {@link #mFragments} 에서 해당 position 에 {@link Fragment} 반환 한다.
     *
     * @param position {@link #mFragments} 위치
     *
     * @return {@link Fragment}
     */
    Fragment findFragment(int position) {
        if (mFragments.size() > position) {
            Fragment f = mFragments.get(position);
            if (f != null) {
                String srcTag = f.getTag();
                String dstTag = getTagFormat(f, position);

                if (StringUtil.equals(srcTag, dstTag) == true) {
                    return f;
                }
            }
        }
        return null;
    }

    //======================================================================
    // FragmentItem
    //======================================================================

    public static class FragmentItem {

        //======================================================================
        // Variables
        //======================================================================

        public String mTitle;

        public Fragment mFragment;

        //======================================================================
        // Constructor
        //======================================================================

        public FragmentItem(String title, Fragment fragment) {
            mTitle = title;
            mFragment = fragment;
        }
    }
}
