package com.starnation.android.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.starnation.android.Config;
import com.starnation.android.app.activity.internal.ActivityCycle;
import com.starnation.android.app.activity.internal.helper.CacheInstanceHelper;
import com.starnation.android.util.Logger;
import com.starnation.android.util.PermissionChecker;

import java.util.Arrays;

/*
 * @author lsh
 * @since 2016. 4. 14.
*/
class ActivityDelegate implements ActivityCycle, ActivityCompat.OnRequestPermissionsResultCallback {

    //======================================================================
    // Constants
    //======================================================================

    private final static String LOG_FORMAT = "[%s]: %s";

    //======================================================================
    // Variables
    //======================================================================

    Activity mActivity;

    PermissionChecker mPermissionChecker;

    CacheInstanceHelper mCacheInstanceHelper;

    //======================================================================
    // Constructor
    //======================================================================

    public ActivityDelegate(@NonNull Activity activity) {
        mActivity = activity;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mPermissionChecker = new PermissionChecker(mActivity);
        cycle("onCreate -> savedInstanceState : " + savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        cycle("onCreate -> savedInstanceState : " + savedInstanceState + " persistentState : " + persistentState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        cycle("onSaveInstanceState -> outState : " + outState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        cycle("onSaveInstanceState -> outState : " + outState + " persistentState : " + outPersistentState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        cycle("onRestoreInstanceState -> savedInstanceState : " + savedInstanceState);
    }

    @Override
    public void onStart() {
        cycle("onStart");
    }

    @Override
    public void onResume() {
        cycle("onResume");
    }

    @Override
    public void onPostResume() {
        cycle("onPostResume");
    }

    @Override
    public void onStop() {
        cycle("onStop");
    }

    @Override
    public void onDestroy() {
        cycle("onDestroy");
        mActivity = null;
        if (mPermissionChecker != null) {
            mPermissionChecker.release();
            mPermissionChecker = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPermissionChecker.onRequestPermissionsResult(requestCode, permissions, grantResults);
        cycle("onRequestPermissionsResult -> requestCode : " + requestCode + " requestCode : " + requestCode + " grantResults : " + Arrays.toString(grantResults));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        cycle("onActivityResult -> requestCode : " + requestCode + " requestCode : " + requestCode + " data : " + data);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public PermissionChecker getPermissionChecker() {
        return mPermissionChecker;
    }

    //======================================================================
    // Methods
    //======================================================================

    void cycle(String message) {
        if (mActivity == null) {
            return;
        }
        if (Config.isLogEnable()) {
            String objectName = mActivity.getClass().getSimpleName();
            Logger.i("ACTIVITY", String.format(LOG_FORMAT, objectName, message));
        }
    }

    Activity getActivity() {
        return mActivity;
    }
}
