package com.starnation.android.animator;

import android.view.animation.Interpolator;

import java.lang.ref.WeakReference;

public abstract class SupportAnimator {

    WeakReference<SupportAnimatorListener> mTarget;

    public SupportAnimator() {

    }

    public SupportAnimator(SupportAnimatorListener target) {
        mTarget = new WeakReference<>(target);
    }

    public abstract boolean isNativeAnimator();

    public abstract Object get();

    public abstract void start();

    public abstract void setDuration(int duration);

    public abstract void setRepeat(int repeat);

    public abstract void setInterpolator(Interpolator value);

    public abstract void addListener(AnimatorListener listener);

    public abstract boolean isRunning();

    public abstract void cancel();


    public void end() {
    }


    public void setupStartValues() {
    }

    public void setupEndValues() {
    }

    public interface AnimatorListener {

        void onAnimationStart();

        void onAnimationEnd();

        void onAnimationCancel();

        void onAnimationRepeat();
    }
}
