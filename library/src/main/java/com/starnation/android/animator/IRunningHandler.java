package com.starnation.android.animator;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/
public interface IRunningHandler {

    boolean isRunning();
}
