package com.starnation.android.animator;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/

import android.view.animation.Interpolator;

import java.lang.ref.WeakReference;

public class PreLSupportAnimator extends SupportAnimator {

    //======================================================================
    // Variables
    //======================================================================

    WeakReference<com.nineoldandroids.animation.Animator> mAnimator;

    //======================================================================
    // Constructor
    //======================================================================

    public PreLSupportAnimator(com.nineoldandroids.animation.Animator animator, SupportAnimatorListener target) {
        super(target);
        setAnimator(animator);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean isNativeAnimator() {
        return false;
    }

    @Override
    public Object get() {
        return mAnimator.get();
    }

    @Override
    public void start() {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.start();
        }
    }

    @Override
    public void setDuration(int duration) {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setDuration(duration);
        }
    }

    @Override
    public void setRepeat(int repeat) {
//        Animator a = mAnimator.get();
//        if (a != null) {
//            a.setDuration(duration);
//        }
    }

    @Override
    public void setInterpolator(Interpolator value) {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setInterpolator(value);
        }
    }


    @Override
    public void addListener(final AnimatorListener listener) {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a == null) {
            return;
        }

        if (listener == null) {
            a.addListener(null);
            return;
        }

        a.addListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(com.nineoldandroids.animation.Animator animation) {
                listener.onAnimationStart();
            }

            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                listener.onAnimationEnd();
            }

            @Override
            public void onAnimationCancel(com.nineoldandroids.animation.Animator animation) {
                listener.onAnimationCancel();
            }

            @Override
            public void onAnimationRepeat(com.nineoldandroids.animation.Animator animation) {
                listener.onAnimationRepeat();
            }
        });
    }

    @Override
    public boolean isRunning() {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        return a != null && a.isRunning();
    }

    @Override
    public void cancel() {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.cancel();
        }
    }

    @Override
    public void end() {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.end();
        }
    }

    @Override
    public void setupStartValues() {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setupStartValues();
        }
    }

    @Override
    public void setupEndValues() {
        com.nineoldandroids.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setupEndValues();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void setAnimator(com.nineoldandroids.animation.Animator animator) {
        mAnimator = new WeakReference<>(animator);
    }
}
