package com.starnation.android.animator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Point;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

/*
 * @author lsh
 * @since 14. 12. 30.
*/
public class AnimationFactory {

    //======================================================================
    // Public Methods
    //======================================================================

    public static void show(final View view) {
        if (view != null && view.getVisibility() != View.VISIBLE) {
            final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
            view.setAlpha(0f);
            view.setVisibility(View.VISIBLE);
            view.animate().alpha(1.0f).setDuration(duration).setListener(null);
        }
    }

    public static void show(final View view, int duration) {
        if (view != null && view.getVisibility() != View.VISIBLE) {
            view.setAlpha(0f);
            view.setVisibility(View.VISIBLE);
            view.animate().alpha(1.0f).setDuration(duration).setListener(null);
        }
    }

    public static void hide(final View view) {
        if (view != null && view.getVisibility() != View.GONE) {
            final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
            view.animate().alpha(1.0f).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.GONE);
                }
            });
        }
    }

    public static void hide(final View view, int duration) {
        if (view != null && view.getVisibility() != View.GONE) {
            view.animate().alpha(1.0f).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.GONE);
                }
            });
        }
    }

    public static void invisible(final View view) {
        if (view != null && view.getVisibility() != View.INVISIBLE) {
            final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
            view.animate().alpha(1.0f).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public static void rotation(final View view, Animation.AnimationListener listener) {
        if (view != null && view.getVisibility() != View.INVISIBLE) {
            Animation an = new RotateAnimation(0.0f, 359,  Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

            final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
            an.setDuration(duration * 7);
            an.setRepeatCount(1);
            an.setFillAfter(false);              // DO NOT keep rotation after animation
            an.setFillEnabled(true);             // Make smooth ending of Animation\
            an.setInterpolator(new LinearInterpolator());

            if (listener == null) {
                an.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }
                });
            } else {
                an.setAnimationListener(listener);
            }

            view.startAnimation(an);
        }
    }

    public static void move(final View view, Point start, Point end, AnimatorListenerAdapter adapter) {
        if (view != null && view.getVisibility() != View.INVISIBLE) {
            final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime) * 10;
            view.animate().translationXBy(end.x - start.x).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
            view.animate().translationYBy(end.y - start.y).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
//            Animation an = new RotateAnimation(0.0f, 359,  Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//
//            final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
//            an.setDuration(duration * 7);
//            an.setRepeatCount(1);
//            an.setFillAfter(false);              // DO NOT keep rotation after animation
//            an.setFillEnabled(true);             // Make smooth ending of Animation\
//            an.setInterpolator(new LinearInterpolator());
//
//            if (listener == null) {
//                an.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//
//                    }
//                });
//            } else {
//                an.setAnimationListener(listener);
//            }
//
//            view.startAnimation(an);
        }
    }

    public static void fadeInMove(final View view, Point start, Point end, Animation.AnimationListener listener) {

//        AnimatorSet aniset = new AnimatorSet();
//        List<Animator> animators = new ArrayList<>();
//        ObjectAnimator translationX = ObjectAnimator.ofFloat(view, "translationX", start.x, end.x);
//        translationX.setInterpolator(new StepInterpolator());
//        animators.add(translationX);
//        ObjectAnimator translationY = ObjectAnimator.ofFloat(view, "translationY", start.y, end.y);
//        translationY.setInterpolator(new StepInterpolator());
//        animators.add(translationY);
//        aniset.playTogether(animators);
//        aniset.setDuration(200);
//        aniset.addListener(listener);
//        aniset.start();
        final int duration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime) * 3;
        Animation move = new TranslateAnimation(start.x, end.x, start.y, end.y);
        move.setDuration(duration * 2);
        move.setRepeatCount(0);
        move.setFillAfter(false);
        move.setFillEnabled(true);
        move.setAnimationListener(listener);
        move.setInterpolator(new LinearInterpolator());
        view.startAnimation(move);
//        Animation alpha = new AlphaAnimation(0f, 1f);
//        alpha.setDuration(duration);
//        alpha.setRepeatCount(0);
//        alpha.setFillAfter(false);
//        alpha.setAnimationListener(listener);
//        view.startAnimation(alpha);

//        aniset.addAnimation(move);
//        Animation alpha = new AlphaAnimation(0f, 1f);
//        aniset.addAnimation(alpha);
//        aniset.setDuration(duration);
//        aniset.setRepeatCount(0);
//        aniset.setFillAfter(false);
//        aniset.setFillEnabled(true);
//        aniset.setAnimationListener(listener);
//
//        view.startAnimation(aniset);
    }
}
