package com.starnation.android.animator;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.animation.Interpolator;

import java.lang.ref.WeakReference;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LollipopSupportAnimator extends SupportAnimator {

    //======================================================================
    // Variables
    //======================================================================

    WeakReference<android.animation.Animator> mAnimator;

    //======================================================================
    // Constructor
    //======================================================================

    public LollipopSupportAnimator(android.animation.Animator animator, SupportAnimatorListener target) {
        super(target);
        mAnimator = new WeakReference<>(animator);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean isNativeAnimator() {
        return true;
    }

    @Override
    public Object get() {
        return mAnimator.get();
    }

    @Override
    public void start() {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.start();
        }
    }

    @Override
    public void setDuration(int duration) {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setDuration(duration);
        }
    }

    @Override
    public void setRepeat(int repeat) {
    }

    @Override
    public void setInterpolator(Interpolator value) {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setInterpolator(value);
        }
    }

    @Override
    public void addListener(final AnimatorListener listener) {
        android.animation.Animator a = mAnimator.get();
        if (a == null) {
            return;
        }

        if (listener == null) {
            a.addListener(null);
            return;
        }

        a.addListener(new android.animation.Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(android.animation.Animator animation) {
                listener.onAnimationStart();
            }

            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                listener.onAnimationEnd();
            }

            @Override
            public void onAnimationCancel(android.animation.Animator animation) {
                listener.onAnimationCancel();
            }

            @Override
            public void onAnimationRepeat(android.animation.Animator animation) {
                listener.onAnimationRepeat();
            }
        });
    }

    @Override
    public boolean isRunning() {
        android.animation.Animator a = mAnimator.get();
        return a != null && a.isRunning();
    }

    @Override
    public void cancel() {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.cancel();
        }
    }

    @Override
    public void end() {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.end();
        }
    }

    @Override
    public void setupStartValues() {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setupStartValues();
        }
    }

    @Override
    public void setupEndValues() {
        android.animation.Animator a = mAnimator.get();
        if (a != null) {
            a.setupEndValues();
        }
    }

    //======================================================================
    // Public Method
    //======================================================================

    public void setAnimator(android.animation.Animator animator) {
        mAnimator = new WeakReference<>(animator);
    }

}
