package com.starnation.android.animator.property;

import com.nineoldandroids.util.FloatProperty;
import com.starnation.android.animator.IRunningHandler;
import com.starnation.android.animator.SupportAnimatorListener;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/
public class PreLFloatProperty extends FloatProperty<SupportAnimatorListener> {

    //======================================================================
    // Variables
    //======================================================================

    private IRunningHandler mRunningHandler;

    //======================================================================
    // Constructor
    //======================================================================

    public PreLFloatProperty(IRunningHandler runningHandler) {
        super("PreLFloatProperty");
        mRunningHandler = runningHandler;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setValue(SupportAnimatorListener object, float value) {
        if (mRunningHandler.isRunning())
            object.onSupportAnimationUpdate();
    }

    @Override
    public Float get(SupportAnimatorListener object) {
        return 0f;
    }
}
