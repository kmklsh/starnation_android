package com.starnation.android.animator;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

import com.nineoldandroids.animation.Animator;

import java.lang.ref.WeakReference;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/
public interface SupportAnimatorListener {

    void onSupportAnimationAttach();

    void onSupportAnimationStart();

    void onSupportAnimationEnd();

    void onSupportAnimationCancel();

    void onSupportAnimationUpdate();

    class FinishedGingerbread implements Animator.AnimatorListener, IRunningHandler {
        WeakReference<SupportAnimatorListener> mReference;
        boolean isRunning = false;

        public FinishedGingerbread(SupportAnimatorListener target) {
            mReference = new WeakReference<>(target);
        }

        @Override
        public void onAnimationStart(Animator animation) {
            SupportAnimatorListener target = mReference.get();
            target.onSupportAnimationStart();
            isRunning = true;
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            SupportAnimatorListener target = mReference.get();
            target.onSupportAnimationCancel();
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            SupportAnimatorListener target = mReference.get();
            target.onSupportAnimationEnd();
        }

        @Override
        public boolean isRunning() {
            return isRunning;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    class FinishedIceCreamSandwich
            extends FinishedGingerbread {
        int mFeaturedLayerType;
        int mLayerType;

        public FinishedIceCreamSandwich(SupportAnimatorListener target) {
            super(target);
            if (target instanceof View) {
                mLayerType = ((View) target).getLayerType();
            }
            mFeaturedLayerType = View.LAYER_TYPE_SOFTWARE;
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            ((View) mReference.get()).setLayerType(mLayerType, null);
            super.onAnimationEnd(animation);
        }

        @Override
        public void onAnimationStart(Animator animation) {
            ((View) mReference.get()).setLayerType(mFeaturedLayerType, null);
            super.onAnimationStart(animation);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            ((View) mReference.get()).setLayerType(mLayerType, null);
            super.onAnimationEnd(animation);
        }
    }

    class AnimatorFinishedJellyBeanMr2 extends FinishedIceCreamSandwich {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public AnimatorFinishedJellyBeanMr2(SupportAnimatorListener target) {
            super(target);

            mFeaturedLayerType = View.LAYER_TYPE_HARDWARE;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    class FinishedLollipop
            implements android.animation.Animator.AnimatorListener, IRunningHandler {
        WeakReference<SupportAnimatorListener> mReference;
        boolean isRunning = false;

        public FinishedLollipop(SupportAnimatorListener target) {
            mReference = new WeakReference<>(target);
        }

        @Override
        public void onAnimationStart(android.animation.Animator animation) {
            if (mReference.get() != null) mReference.get().onSupportAnimationStart();
            isRunning = true;
        }

        @Override
        public void onAnimationEnd(android.animation.Animator animation) {
            if (mReference.get() != null) mReference.get().onSupportAnimationEnd();
        }

        @Override
        public void onAnimationCancel(android.animation.Animator animation) {
            if (mReference.get() != null) mReference.get().onSupportAnimationCancel();
        }

        @Override
        public void onAnimationRepeat(android.animation.Animator animation) {

        }

        @Override
        public boolean isRunning() {
            return isRunning;
        }
    }
}
