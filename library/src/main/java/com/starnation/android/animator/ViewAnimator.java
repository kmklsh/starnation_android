package com.starnation.android.animator;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * @author Saeed Masoumi
 */
public final class ViewAnimator {

  public static ViewAnimator with(View view) {
    return new ViewAnimator(view);
  }

  private View mView;

  private ViewAnimator(View view) {
    mView = view;
  }

  public void moveTo(int duration, float toX, float toY) {
    moveTo(duration, 0, toX, 0, toY);
  }

  public void moveTo(int duration, float fromX, float toX, float fromY, final float toY) {
    AnimatorSet animatorSet = new AnimatorSet();
    animatorSet.setDuration(duration);
    animatorSet.playTogether(ObjectAnimator.ofFloat(mView, "translationX", fromX, toX),
        ObjectAnimator.ofFloat(mView, "translationY", fromY, toY));
    animatorSet.start();
  }
}
