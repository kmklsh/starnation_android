package com.starnation.android.animator.property;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Property;

import com.starnation.android.animator.IRunningHandler;
import com.starnation.android.animator.SupportAnimatorListener;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class LollipopFloatProperty extends Property<SupportAnimatorListener, Float> {

    //======================================================================
    // Variables
    //======================================================================

    private IRunningHandler mRunningHandler;

    //======================================================================
    // Constructor
    //======================================================================

    public LollipopFloatProperty(IRunningHandler runningHandler) {
        super(Float.class, "LollipopFloatProperty");
        mRunningHandler = runningHandler;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void set(SupportAnimatorListener object, Float value) {
        if (mRunningHandler.isRunning())
            object.onSupportAnimationUpdate();
    }

    @Override
    public Float get(SupportAnimatorListener object) {
        return 0f;
    }
}
