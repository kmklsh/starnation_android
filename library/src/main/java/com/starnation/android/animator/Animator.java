package com.starnation.android.animator;

import android.animation.ObjectAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.starnation.android.animator.property.LollipopFloatProperty;
import com.starnation.android.animator.property.PreLFloatProperty;

import static android.os.Build.VERSION.SDK_INT;

/*
 * @author lsh
 * @since 2016. 4. 25.
*/
public final class Animator {

    //======================================================================
    // Constants
    //======================================================================

    private static final String PROPERTY = "SupportAnimatorProperty";

    private static boolean LOLLIPOP_SUPPORT =
            SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH;

    //======================================================================
    // Constructor
    //======================================================================

    public static SupportAnimator with(SupportAnimatorListener listener, int duration) {
        SupportAnimator supportAnimator = createAnimator(listener);
        supportAnimator.setDuration(duration);
        supportAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        return supportAnimator;
    }

    private static SupportAnimator createAnimator(final SupportAnimatorListener listener) {
        listener.onSupportAnimationAttach();
        if (LOLLIPOP_SUPPORT) {
            android.animation.Animator.AnimatorListener animListener =
                    getLollipopFinishedListener(listener);
            ObjectAnimator animator = ObjectAnimator.ofFloat(listener,
                    new LollipopFloatProperty((IRunningHandler) animListener), 0);
            animator.addListener(animListener);
            return new LollipopSupportAnimator(animator, listener);
        } else {
            com.nineoldandroids.animation.Animator.AnimatorListener animListener =
                    getFinishListener(listener);
            com.nineoldandroids.animation.ObjectAnimator animator =
                    com.nineoldandroids.animation.ObjectAnimator.ofFloat(listener,
                            new PreLFloatProperty((IRunningHandler) animListener), 0);
            animator.addListener(animListener);
            return new PreLSupportAnimator(animator, listener);
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static com.nineoldandroids.animation.Animator.AnimatorListener getFinishListener(
            SupportAnimatorListener target) {
        if (SDK_INT >= 18) {
            return new SupportAnimatorListener.AnimatorFinishedJellyBeanMr2(target);
        } else if (SDK_INT >= 14) {
            return new SupportAnimatorListener.FinishedIceCreamSandwich(target);
        } else {
            return new SupportAnimatorListener.FinishedGingerbread(target);
        }
    }

    private static android.animation.Animator.AnimatorListener getLollipopFinishedListener(
            SupportAnimatorListener listener) {
        return new SupportAnimatorListener.FinishedLollipop(listener);
    }

}
