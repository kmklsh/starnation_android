package com.starnation.android.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;

/*
 * @author lsh
 * @since 14. 8. 19.
 */
public final class NetworkChecker {

    /**
     * WIFI 연결 체크
     *
     * @param context
     *
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        return isConnected(context, ConnectivityManager.TYPE_WIFI);
    }

    /**
     * 3G 연결 체크
     *
     * @param context
     *
     * @return
     */
    public static boolean isConnectedMobile(Context context) {
        return isConnected(context, ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * 4G(LTE, WIMAX, WIBRO) 연결 체크
     *
     * @param context
     *
     * @return
     */
    public static boolean isConnectedWimax(Context context) {
        return isConnected(context, ConnectivityManager.TYPE_WIMAX);
    }

    /**
     * 통신 연결 상태 체크
     *
     * @return
     */
    public static boolean isConnected(Context context) {
        return isConnectedWifi(context) || isConnectedMobile(context) || isConnectedWimax(context);
    }

    /**
     * 통신 연결 상태 체크
     *
     * @param context     context
     * @param networkType ConnectivityManager 에서 TYPE_MOBILE, TYPE_WIFI, TYPE_WIMAX 값으로 체크
     *
     * @return TYPE_MOBILE | TYPE_WIFI | TYPE_WIMAX 하나라도 체크되면 true 아니면 false
     */
    private static boolean isConnected(Context context, int networkType) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo networkInfo = manager.getNetworkInfo(networkType);
            if (networkInfo != null) {
                return networkInfo.isConnectedOrConnecting();
            }
        }
        return false;
    }

    /**
     * WIFI 설정 화면 전환
     */
    public static void goWifiSetting(Context context) {
        Intent networkConSettingIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        networkConSettingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(networkConSettingIntent);
    }

    /**
     * Mobile Network 설정 화면 전환
     */
    public static void goMobileNetworkSetting(Context context) {
        Intent networkConSettingIntent = new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS);
        networkConSettingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(networkConSettingIntent);
    }

    /**
     * WIFI Mac Address 가져온다.
     * 한번리아도 WIFI 접속을 하면 단말 상태가 네트워크로 접속 하여도 Mac 주소를 가져옴
     * 예외로 Samsung Note1 단말이나 일부 LG 단말에서는 WIFI 연결해야 Mac 주소를 가져오 올수 있다.
     *
     * @return Mac 주소 없으면 공백으로 표시
     */
    public static String getMacAddress(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            return wInfo.getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 비행기 모드 체크 Build.VERSION_CODES.JELLY_BEAN_MR1 이상인경우 처리 방식에 약간 차이 있음
     *
     * @return 비행기 모드면 true
     */
    public static boolean isAirplaneMode(Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
            } else {
                return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
