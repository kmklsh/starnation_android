package com.starnation.android.content;

import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.starnation.android.app.ApplicationBehavior;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public final class IntentFactory {

    /**
     * 최근 실행한 Activity
     *
     * @param context
     *
     * @return
     */
    public static Intent createRecentlyExecutedActivity(Context context) {
        ActivityManager.RunningTaskInfo info = ApplicationBehavior.getTopRunningTaskInfo(context);
        if (ApplicationBehavior.isRunningProcess(context, context.getPackageName()) == true && info != null && info.topActivity != null) {
            Intent intent = new Intent();
            intent.setClassName(context, info.topActivity.getClassName());
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            return intent;
        } else {
            return context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        }
    }

    /**
     * GPS 설정 화면으로 이동
     *
     * @param context
     */
    public static void goGpsSetting(Context context) {
        Intent gpsSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(gpsSettingIntent);
    }

    public static void goShare(Context context, String extraText) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (extraText != null) {
            sendIntent.putExtra(Intent.EXTRA_TEXT, extraText);
        }
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, "공유"));
    }

    public static void goApplicationDetailsSetting(@NonNull Context context) {
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + context.getPackageName()));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            context.startActivity(intent);
        }
    }
}
