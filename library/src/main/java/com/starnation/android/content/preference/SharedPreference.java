package com.starnation.android.content.preference;

/*
 * @author lsh
 * @since 14. 11. 24.
*/
public interface SharedPreference {

    /**
     * Preference 저장처리
     *
     * @return
     */
    boolean save();

    /**
     * reference 데이터 삭제
     *
     * @return
     */
    boolean clear();

    /**
     * Preference key 해당되는 값 삭제
     *
     * @param key
     *
     * @return
     */
    boolean remove(String key);

    /**
     * boolean 값 저장 save 메소드 자동 실행
     *
     * @param key
     * @param value
     */
    SharedPreference putBoolean(String key, boolean value);

    /**
     * int 값 저장 save 메소드 자동 실행
     *
     * @param key
     * @param value
     */
    SharedPreference putInteger(String key, int value);

    /**
     * long 값 저장 save 메소드 자동 실행
     *
     * @param key
     * @param value
     */
    SharedPreference putLong(String key, long value);

    /**
     * float 값 저장 save 메소드 자동 실행
     *
     * @param key
     * @param value
     */
    SharedPreference putFloat(String key, float value);

    /**
     * String 값 저장 save 메소드 자동 실행
     *
     * @param key
     * @param value
     */
    SharedPreference putString(String key, String value);

    /**
     * String 값 반환
     *
     * @param key
     * @param value
     *
     * @return
     */
    String getString(String key, String value);

    /**
     * Boolean 값 반환
     *
     * @param key
     * @param value
     *
     * @return
     */
    boolean getBoolean(String key, boolean value);

    /**
     * Int 값 반환
     *
     * @param key
     * @param value
     *
     * @return
     */
    int getInteger(String key, int value);

    /**
     * Long 값 반환
     *
     * @param key
     * @param value
     *
     * @return
     */
    long getLong(String key, long value);

    /**
     * Float 값 반환
     *
     * @param key
     * @param value
     *
     * @return
     */
    float getFloat(String key, float value);

    /**
     * 해당 키값이 있는지 반환
     *
     * @param key
     *
     * @return
     */
    boolean contains(String key);

}
