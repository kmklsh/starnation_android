package com.starnation.android.content.preference;

import android.annotation.SuppressLint;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.SharedPreferences;

/*
 * @author lsh
 * @since 14. 11. 24.
*/
public class DefaultSharedPreference implements SharedPreference {

    //======================================================================
    // Variables
    //======================================================================

    private SharedPreferences mSharedPreferences;

    private BackupManager mBackupManager;

    private SharedPreferences.Editor mEditor;

    //======================================================================
    // Constructor
    //======================================================================

    public static DefaultSharedPreference create(Context context, String sharedPreferenceFileName) {
        return new DefaultSharedPreference(context, sharedPreferenceFileName);
    }

    @SuppressLint("CommitPrefEdits")
    @Deprecated
    public DefaultSharedPreference(Context context, String sharedPreferenceFileName) {
        mSharedPreferences = context.getSharedPreferences(sharedPreferenceFileName, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mBackupManager = new BackupManager(context);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean save() {
        try {
            mEditor.apply();
            mBackupManager.dataChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean clear() {
        mEditor.clear();
        return save();
    }

    @Override
    public boolean remove(String key) {
        mEditor.remove(key);
        return save();
    }

    @Override
    public DefaultSharedPreference putBoolean(String key, boolean value) {
        mEditor.putBoolean(key, value);
        return this;
    }

    @Override
    public DefaultSharedPreference putInteger(String key, int value) {
        mEditor.putInt(key, value);
        return this;
    }

    @Override
    public DefaultSharedPreference putLong(String key, long value) {
        mEditor.putLong(key, value);
        return this;
    }

    @Override
    public DefaultSharedPreference putFloat(String key, float value) {
        mEditor.putFloat(key, value);
        return this;
    }

    @Override
    public DefaultSharedPreference putString(String key, String value) {
        mEditor.putString(key, value);
        return this;
    }

    @Override
    public String getString(String key, String value) {
        return mSharedPreferences.getString(key, value);
    }

    @Override
    public boolean getBoolean(String key, boolean value) {
        return mSharedPreferences.getBoolean(key, value);
    }

    @Override
    public int getInteger(String key, int value) {
        return mSharedPreferences.getInt(key, value);
    }

    @Override
    public long getLong(String key, long value) {
        return mSharedPreferences.getLong(key, value);
    }

    @Override
    public float getFloat(String key, float value) {
        return mSharedPreferences.getFloat(key, value);
    }

    @Override
    public boolean contains(String key) {
        return mSharedPreferences.contains(key);
    }
}
