package com.starnation.android.content;

import android.content.Context;
import android.content.pm.PackageManager;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
@Deprecated
public final class ApplicationInfo {

    /**
     * packageName 으로 Application 설치 여부 판단
     *
     * @param context
     * @param packageName
     *
     * @return Application 설칠 되어 있으면 true 리턴
     */
    public static boolean isInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    /**
     * Application 버전 확인
     *
     * @param context
     * @param packageName packageName null 이면 Context 에서 getPackageName 가져 온다
     *
     * @return 버전 리턴 없으면 공백으로 표시한다.
     */
    public static String getVersionName(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        String version = "";
        try {
            version = pm.getPackageInfo(packageName == null ? context.getPackageName() : packageName, PackageManager.GET_META_DATA).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * Application 버전 코드 확인
     *
     * @param context
     * @param packageName packageName null 이면 Context 에서 getPackageName 가져 온다
     *
     * @return 버전 코드 없으면 0 표시한다.
     */
    public static int getVersionCode(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        int versionCode = 0;
        try {
            versionCode = pm.getPackageInfo(packageName == null ? context.getPackageName() : packageName, PackageManager.GET_META_DATA).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }
}
