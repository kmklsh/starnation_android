package com.starnation.android.text;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/*
 * @author lsh
 * @since 2016. 2. 25.
*/
public final class NumberFormatFactory {

    public static String makeDecimalFormatString(String format, String suffix, int value) {
        NumberFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(value) + suffix;
    }

    public static String makeDecimalFormatString(String format, String suffix, long value) {
        NumberFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(value) + suffix;
    }

    public static String makeDecimalFormatString(String format, String suffix, double value) {
        NumberFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(value) + suffix;
    }

    public static String makeDecimalFormatString(String format, String suffix, float value) {
        NumberFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(value) + suffix;
    }
}
