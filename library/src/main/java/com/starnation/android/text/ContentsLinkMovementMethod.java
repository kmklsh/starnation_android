package com.starnation.android.text;

import android.content.Context;

/*
 * @author lsh
 * @since 15. 1. 6.
*/
public class ContentsLinkMovementMethod extends LinkMovementMethod {

    //======================================================================
    // Variables
    //======================================================================

    public final static ContentsLinkMovementMethod INSTANCE = new ContentsLinkMovementMethod();

    //======================================================================
    // Constructor
    //======================================================================

    public static ContentsLinkMovementMethod getInstance() {
        return INSTANCE;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @Override
    public void onUrl(Context context, String url) {
//        IntentBrowserFactory.goContentsLink(context, url);
    }

}
