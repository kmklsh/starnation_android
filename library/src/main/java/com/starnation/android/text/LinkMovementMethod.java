package com.starnation.android.text;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.text.style.URLSpan;
import android.view.MotionEvent;

/*
 * @author lsh
 * @since 15. 1. 6.
*/
public class LinkMovementMethod extends android.text.method.LinkMovementMethod {

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean onTouchEvent(@NonNull android.widget.TextView widget, @NonNull android.text.Spannable buffer, @NonNull MotionEvent event) {
        int action = event.getAction();

        if (action == MotionEvent.ACTION_UP) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();

            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            URLSpan[] link = buffer.getSpans(off, off, URLSpan.class);
            if (link.length != 0) {
                String url = link[0].getURL();
                onUrl(widget.getContext(), url);
                return true;
            }
        }
        return super.onTouchEvent(widget, buffer, event);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void onUrl(Context context, String url) {
    }

}
