package com.starnation.android.text;

import android.text.InputFilter;
import android.text.Spanned;

import java.io.UnsupportedEncodingException;

/*
 * @author lsh
 * @since 14. 12. 11.
*/
public abstract class ByteLengthFilter implements InputFilter {


    //======================================================================
    // Abstract Method
    //======================================================================

    public abstract int getMaxByte();

    public abstract String getCharset();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String expected = "";
        expected += dest.subSequence(0, dstart);
        expected += source.subSequence(start, end);
        expected += dest.subSequence(dend, dest.length());

        int keep = calculateMaxLength(expected) - (dest.length() - (dend - dstart));

        if (keep <= 0) {
            return ""; // source 입력 불가(원래 문자열 변경 없음)
        } else if (keep >= end - start) {
            return null; // keep original. source 그대로 허용
        } else {
            return source.subSequence(start, start + keep); // source중 일부만 입력 허용
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    /**
     입력가능한 최대 문자 길이(최대 바이트 길이와 다름!).
     */
    private int calculateMaxLength(String expected) {
        return getMaxByte() - (getByteLength(expected) - expected.length());
    }

    private int getByteLength(String str) {
        try {
            return str.getBytes(getCharset()).length;
        } catch (UnsupportedEncodingException ignored) {
        }
        return 0;
    }

}
