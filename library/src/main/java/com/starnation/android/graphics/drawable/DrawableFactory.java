package com.starnation.android.graphics.drawable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.starnation.android.graphics.drawable.card.CardDrawable;

/*
 * @author lsh
 * @since 2016. 2. 25.
*/
public final class DrawableFactory {

    //======================================================================
    // Public Methods
    //======================================================================

    @SuppressWarnings("ConstantConditions")
    public static Drawable createDrawable(@NonNull Context context, @DrawableRes int drawable, float scaleWidth, float scaleHeight) {
        Drawable src = ContextCompat.getDrawable(context, drawable);
        if (src != null) {
            try {
                final int width = Math.round(src.getIntrinsicWidth() * scaleWidth);
                final int height = Math.round(src.getIntrinsicHeight() * scaleHeight);
                return createBitmapDrawable(context, src, width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static StateListDrawable createStateListDrawable(@NonNull Drawable drawableChecked, @NonNull Drawable drawableUnChecked, @NonNull Drawable drawableDisable) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{android.R.attr.state_checked}, drawableChecked);
        stateListDrawable.addState(new int[]{-android.R.attr.state_checked}, drawableUnChecked);
        stateListDrawable.addState(new int[]{-android.R.attr.state_enabled}, drawableDisable);
        return stateListDrawable;
    }

    public static CardDrawable createCardDrawable(Context context, int backgroundColor, float radius) {
        return new CardDrawable(context.getResources(), backgroundColor, radius);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static BitmapDrawable createBitmapDrawable(Context context, Drawable drawable, int resizeWidth, int resizeHeight) throws Exception {
        Bitmap bitmap = createBitmap(drawable);
        if (bitmap == null) {
            throw new NullPointerException("bitmap null");
        }
        if (resizeWidth <= 0 || resizeHeight <= 0) {
            throw new IllegalArgumentException("resizeWidth <= 0 of resizeHeight <= 0");
        }
        float scaleX = (float) resizeWidth / (float) bitmap.getWidth();
        float scaleY = (float) resizeHeight / (float) bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(scaleX, scaleY);
        Bitmap resizeBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return new BitmapDrawable(context.getResources(), resizeBitmap);
    }


    /**
     * Drawable 을 Bitmap 으로 리턴
     *
     * @param drawable drawable
     *
     * @return Bitmap
     *
     * @throws Exception Drawable null 일경우를 NullPointerException drawable width <= 0 of drawable height <= 0 IllegalArgumentException throws 한다.
     */
    private static Bitmap createBitmap(Drawable drawable) throws Exception {
        if (drawable == null) {
            throw new NullPointerException("drawable null");
        }
        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            throw new IllegalArgumentException("drawable width <= 0 of drawable height <= 0");
        }
        final int width = drawable.getIntrinsicWidth();
        final int height = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

}

