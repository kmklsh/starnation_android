package com.starnation.android.graphics.drawable;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

/*
 * @author lsh
 * @since 2016. 10. 13.
*/
public final class DrawableHelper {

    //======================================================================
    // Variables
    //======================================================================

    @ColorInt
    private int mColor;

    private Drawable mDrawable;

    private Drawable mWrappedDrawable;

    //======================================================================
    // Constructor
    //======================================================================

    public DrawableHelper(Drawable drawable) {
        mDrawable = drawable;
    }

    public static DrawableHelper with(@NonNull Drawable drawable) {
        return new DrawableHelper(drawable);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public DrawableHelper color(@ColorInt int color) {
        mColor = color;
        return this;
    }

    public DrawableHelper tint() {
        if (mDrawable == null) {
            throw new NullPointerException("Drawable Null");
        }

        if (mColor == 0) {
            throw new IllegalStateException("Color 0");
        }

        mWrappedDrawable = mDrawable.mutate();
        mWrappedDrawable = DrawableCompat.wrap(mWrappedDrawable);
        DrawableCompat.setTint(mWrappedDrawable, mColor);
        DrawableCompat.setTintMode(mWrappedDrawable, PorterDuff.Mode.SRC_IN);

        return this;
    }

    @SuppressWarnings("deprecation")
    public void applyToBackground(@NonNull View view) {
        if (mWrappedDrawable == null) {
            throw new NullPointerException("mWrappedDrawable Null");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(mWrappedDrawable);
        } else {
            view.setBackgroundDrawable(mWrappedDrawable);
        }
    }

    public void applyTo(@NonNull ImageView imageView) {
        if (mWrappedDrawable == null) {
            throw new NullPointerException("mWrappedDrawable Null");
        }
        imageView.setImageDrawable(mWrappedDrawable);
    }

    public void applyTo(@NonNull MenuItem menuItem) {
        if (mWrappedDrawable == null) {
            throw new NullPointerException("mWrappedDrawable Null");
        }
        menuItem.setIcon(mWrappedDrawable);
    }

    public Drawable get() {
        if (mWrappedDrawable == null) {
            throw new NullPointerException("mWrappedDrawable Null");
        }
        return mWrappedDrawable;
    }
}
