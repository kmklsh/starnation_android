package com.starnation.android.graphics.bitmap;

import android.graphics.Bitmap;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public interface Transformation {

    Bitmap transform(Bitmap bitmap);
}
