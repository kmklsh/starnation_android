package com.starnation.android.graphics.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;

import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 15. 4. 8.
*/
public final class BadgeDrawable extends android.graphics.drawable.ShapeDrawable {

    //======================================================================
    // Constants
    //======================================================================

    private static final int DEFAULT_CORNER_RADIUS_DIP = 8;

    private final static int DEFAULT_MAX_COUNT = 100;

    private final static int DEFAULT_PADDING_DIP = 10;

    //======================================================================
    // Variables
    //======================================================================

    private Paint mTextPaint;

    private Rect mTxtRect = new Rect();

    private final Paint mPaint;

    private boolean mWillDraw;

    private String mCountText = "";

    private int mMaxCount = DEFAULT_MAX_COUNT;

    private final int mPaddingVertical;

    private final int mPaddingHorizontal;

    //======================================================================
    // Constructor
    //======================================================================

    public BadgeDrawable(Context context, int color, int textSize, int textColor) {
        super(new RoundRectShape(getOuterRadii(DEFAULT_CORNER_RADIUS_DIP * context.getResources().getDisplayMetrics().density), null, null));
        mPaddingVertical = (int) (DEFAULT_PADDING_DIP * context.getResources().getDisplayMetrics().density);
        mPaddingHorizontal = (int) (DEFAULT_PADDING_DIP * context.getResources().getDisplayMetrics().density);

        mPaint = new Paint(getPaint());
        mPaint.setColor(color);

        mTextPaint = new Paint();
        mTextPaint.setColor(textColor);
        mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mTextPaint.setTextSize(textSize);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onDraw(Shape shape, Canvas canvas, Paint paint) {
        if (!mWillDraw || StringUtil.isEmpty(mCountText) == true) {
            return;
        }
        shape.draw(canvas, mPaint);

        mTextPaint.getTextBounds(mCountText, 0, mCountText.length(), mTxtRect);
        float textHeight = mTxtRect.bottom - mTxtRect.top;
        float textY = getBounds().centerY() + (textHeight / 2f);

        canvas.drawText(mCountText, getBounds().centerX(), textY, mTextPaint);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setCount(int count) {
        mCountText = count > mMaxCount ? Integer.toString(mMaxCount - 1) + "+" : Integer.toString(count);
        mWillDraw = count > 0;
        mTextPaint.getTextBounds(mCountText, 0, mCountText.length(), mTxtRect);

        setIntrinsicWidth(mTxtRect.width() + mPaddingHorizontal);
        setIntrinsicHeight(mTxtRect.height() + mPaddingVertical);
        invalidateSelf();
    }

    public void setMaxCount(int maxCount) {
        mMaxCount = maxCount;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static float[] getOuterRadii(float radii) {
        return new float[]{radii, radii, radii, radii, radii, radii, radii, radii};
    }
}
