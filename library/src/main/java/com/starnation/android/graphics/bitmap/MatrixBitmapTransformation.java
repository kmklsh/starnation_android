package com.starnation.android.graphics.bitmap;

import android.graphics.Bitmap;
import android.graphics.Matrix;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public class MatrixBitmapTransformation implements Transformation {

    //======================================================================
    // Variables
    //======================================================================

    private final Builder mBuilder;

    //======================================================================
    // Constructor
    //======================================================================

    @Deprecated
    MatrixBitmapTransformation(Builder builder) {
        mBuilder = builder;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public Bitmap transform(Bitmap src) {
        try {
            valid(src);
            if (mBuilder.mRotate == true) {
                Matrix matrix = new Matrix();
                matrix.postRotate(mBuilder.mDegrees);
                return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return src;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void valid(Bitmap bitmap) throws Exception {
        if (bitmap == null) {
            throw new NullPointerException("bitmap null");
        }

        if (mBuilder.mRotate == true) {
            if (mBuilder.mDegrees <= 0 || mBuilder.mDegrees > 360) {
                throw new IllegalArgumentException("rotate <= 0 of rotate > 360");
            }
        }
    }

    //======================================================================
    // Builder
    //======================================================================

    public static class Builder {

        //======================================================================
        // Variables
        //======================================================================

        private float mDegrees;

        private boolean mRotate;

        public Builder setRotate(boolean rotate) {
            mRotate = rotate;
            return this;
        }

        /**
         * @param degrees 각도 0 ~ 360
         */
        public Builder setDegrees(float degrees) {
            mDegrees = degrees;
            return this;
        }

        public MatrixBitmapTransformation build() {
            return new MatrixBitmapTransformation(this);
        }
    }
}
