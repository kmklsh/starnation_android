package com.starnation.android.graphics.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public abstract class BlurBitmapTransformation implements Transformation {

    //======================================================================
    // Constants
    //======================================================================

    private static final int DEFAULT_BLUR_RADIUS = 25;

    //======================================================================
    // Variables
    //======================================================================

    private int mRadius = DEFAULT_BLUR_RADIUS;

    //======================================================================
    // Constructor
    //======================================================================

    public static BlurBitmapTransformation create(final Context context, int radius) {
        return new BlurBitmapTransformation(radius) {
            @Override
            public Context getContext() {
                return context;
            }
        };
    }

    public static BlurBitmapTransformation create(final Context context) {
        return new BlurBitmapTransformation(DEFAULT_BLUR_RADIUS) {
            @Override
            public Context getContext() {
                return context;
            }
        };
    }

    public BlurBitmapTransformation(int radius) {
        mRadius = radius;
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract Context getContext();

    //======================================================================
    // Override Methods
    //======================================================================

    public Bitmap transform(Bitmap src) {
        try {
            valid(src);
            try {
                //Modify 15. 6. 15. lsh SM-N90L Crash bug 발생
                Bitmap bitmap = src.copy(src.getConfig() != null ? src.getConfig() : Bitmap.Config.ARGB_8888, true);
                RenderScript rs = RenderScript.create(getContext());
                Allocation input = Allocation.createFromBitmap(rs, src, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
                Allocation output = Allocation.createTyped(rs, input.getType());
                ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                script.setRadius(mRadius /* e.g. 3.f */);
                script.setInput(input);
                script.forEach(output);
                output.copyTo(bitmap);
                script.destroy();
                output.destroy();
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return src;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void valid(Bitmap bitmap) throws Exception {
        if (bitmap == null) {
            throw new NullPointerException("bitmap null");
        }
    }
}
