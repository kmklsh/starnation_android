package com.starnation.android.graphics.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.starnation.util.StringUtil;

import java.io.File;
import java.io.FileOutputStream;

/*
 * @author lsh
 * @since 2016. 1. 7.
*/
public final class BitmapCache {

    //======================================================================
    // Variables
    //======================================================================

    File mCacheFile;

    File mCacheDirectory;

    boolean mSkipCacheManaged = true;

    //======================================================================
    // Constructor
    //======================================================================

    public static BitmapCache create(@NonNull Context context, @NonNull String dirName, @NonNull String fileName) {
        BitmapCache cacheV2 = new BitmapCache();
        cacheV2.mCacheFile = createCacheFile(context, dirName, fileName);
        cacheV2.mCacheDirectory = createCacheFile(context, dirName, null);
        return cacheV2;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @Nullable
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static File createCacheFile(@NonNull Context context, @NonNull String dirName, String fileName) {
        try {
            File cacheLocation = new File(getImageDirectory(context), dirName);
            if (cacheLocation.isDirectory() == false) {
                cacheLocation.mkdirs();
            }

            if (StringUtil.isEmpty(fileName) == false) {
                File file = new File(cacheLocation, fileName);
                if (file.exists() == false) {
                    file.createNewFile();
                }
                return file;
            }
            return cacheLocation;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public File getCacheFile() {
        return mCacheFile;
    }

    public void writeJPEGFile(@NonNull Bitmap bitmap) {
        writeCacheDirImageFile(mCacheFile, Bitmap.CompressFormat.JPEG, 100, bitmap);
    }

    public void writeJPEGFile(@NonNull Bitmap bitmap, int quality) {
        writeCacheDirImageFile(mCacheFile, Bitmap.CompressFormat.JPEG, quality, bitmap);
    }

    @Nullable
    public Bitmap getBitmap() {
        try {
            return BitmapFactory.decodeFile(mCacheFile.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public boolean delete() {
        return mCacheFile.exists() == true && mCacheFile.delete();
    }

    public boolean deleteDirectory() {
        return mCacheDirectory.exists() == true && mCacheDirectory.delete();
    }

    public boolean isSkipCacheManaged() {
        return mSkipCacheManaged;
    }

    public void setSkipCacheManaged(boolean skipCacheManaged) {
        mSkipCacheManaged = skipCacheManaged;
    }
    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("ConstantConditions")
    private static File getImageDirectory(@NonNull Context context) throws Exception{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            return context.getFilesDir();
        }else{
            return context.getExternalCacheDir();
        }
    }

    private static int getBitmapByteCount(@NonNull Bitmap bitmap) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getRowBytes() * bitmap.getHeight();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getAllocationByteCount();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void writeCacheDirImageFile(@NonNull File file, @NonNull Bitmap.CompressFormat format, int quality, @NonNull Bitmap bitmap) {
        try {
            FileOutputStream stream = new FileOutputStream(file);
            bitmap.compress(format, quality, stream);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
