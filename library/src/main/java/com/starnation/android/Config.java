package com.starnation.android;

/*
 * @author lsh
 * @since 2017. 8. 30.
*/
public final class Config {

    private static boolean sLogEnable;

    public static boolean isLogEnable() {
        return sLogEnable;
    }

    public static void setLogEnable(boolean logEnable) {
        sLogEnable = logEnable;
    }
}
