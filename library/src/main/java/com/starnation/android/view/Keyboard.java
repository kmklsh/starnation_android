package com.starnation.android.view;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/*
 * @author lsh
 * @since 15. 2. 25.
*/
public final class Keyboard {

    public static boolean isShowKeyboard(Context context) {
        return ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).isAcceptingText();
    }

    public static void showKeyboard(View view) {
        if (view != null) {
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);

            if (view instanceof EditText) {
                ((EditText) view).setCursorVisible(true);
            }
        }
    }

    public static void hideKeyboard(View view) {
        if (view != null) {
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            if (view instanceof EditText) {
                ((EditText) view).setCursorVisible(false);
            }
        }
    }

    public static void showSafeKeyboard(final View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showKeyboard(view);
                }
            }, 300);
        }
    }
}
