package com.starnation.android.view;

import android.view.Window;
import android.view.WindowManager;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public final class WindowUtil {

    /**
     * 화면 절전 모드로 안되게 처리
     *
     * @param window
     */
    public static void screenOn(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * 장금화면 활성화 또는 비활성화 처리
     *
     * @param window
     * @param enabled 잠금화면 활성 여부
     */
    public static void screenPatternLock(Window window, boolean enabled) {
        int flag = 0;
        if (!enabled) {
            flag |= WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED; // Lock 화면 위로 실행(full-screen 만 지원)
            flag |= WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD; // Keyguard 를 해지한다.
            window.addFlags(flag);
        } else {
            flag |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON; // Screen 을 켜진 상태로 유지한다.
            flag |= WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON; // Screen On
            flag |= WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON; // Screen On 상태에서 장금 상태 활성화
            window.addFlags(flag);
        }
    }
}
