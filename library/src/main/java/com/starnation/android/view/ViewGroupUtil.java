package com.starnation.android.view;

import android.view.View;
import android.view.ViewGroup;

/*
 * @author lsh
 * @since 2016. 2. 29.
*/
public class ViewGroupUtil {

    public static void setMargin(View view, int left, int top, int right, int bottom) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return;
        }
        params.setMargins(left, top, right, bottom);
    }

    public static void setMarginRight(View view, int right) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return;
        }
        params.setMargins(params.leftMargin, params.topMargin, right, params.bottomMargin);
    }

    public static void setMarginLeft(View view, int left) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return;
        }
        params.setMargins(left, params.topMargin, params.rightMargin, params.bottomMargin);
    }

    public static void setMarginTop(View view, int top) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return;
        }
        params.setMargins(params.leftMargin, top, params.rightMargin, params.bottomMargin);
    }

    public static void setMarginBottom(View view, int bottom) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return;
        }
        params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottom);
    }

    public static int getMarginTop(View view) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return 0;
        }
        return params.topMargin;
    }

    public static int getMarginBottom(View view) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return 0;
        }
        return params.bottomMargin;
    }

    public static int getMarginLeft(View view) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return 0;
        }
        return params.leftMargin;
    }

    public static int getMarginRight(View view) {
        ViewGroup.MarginLayoutParams params = getMarginLayoutParams(view);
        if (params == null) {
            return 0;
        }
        return params.rightMargin;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static ViewGroup.MarginLayoutParams getMarginLayoutParams(View view) {
        if (view != null && view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            return (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        }
        return null;
    }
}
