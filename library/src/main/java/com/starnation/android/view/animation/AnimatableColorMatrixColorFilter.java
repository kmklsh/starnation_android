package com.starnation.android.view.animation;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;

/*
 * @author lsh
 * @since 2016. 4. 20.
*/
public class AnimatableColorMatrixColorFilter {
    private ColorMatrixColorFilter mFilter;

    public AnimatableColorMatrixColorFilter(ColorMatrix matrix) {
        setColorMatrix(matrix);
    }

    public ColorMatrixColorFilter getColorFilter() {
        return mFilter;
    }

    public void setColorMatrix(ColorMatrix matrix) {
        mFilter = new ColorMatrixColorFilter(matrix);
    }
}