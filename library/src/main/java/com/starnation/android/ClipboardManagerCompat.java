package com.starnation.android;

import android.content.Context;
import android.os.Build;
import android.text.ClipboardManager;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public final class ClipboardManagerCompat {

    /**
     * 문자열 ClipboardManager 저장처리
     *
     * @param context
     * @param text    문자열
     */
    public static void clipBoard(Context context, String text) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            ClipboardManager managerLowLevel = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            managerLowLevel.setText(text);
        } else {
            android.content.ClipboardManager manager = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            manager.setText(text);
        }
    }
}
