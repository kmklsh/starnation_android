package com.starnation.sample

import android.os.Bundle
import android.support.v4.app.Fragment
import com.starnation.android.app.fragment.viewmodel.FragmentViewModel
import com.starnation.android.util.Logger
import com.starnation.sample.model.SampleStore

/*
 * @author lsh
 * @since 2017. 8. 30.
*/
class SampleViewModel(fragment: Fragment) : FragmentViewModel(fragment) {

    internal var outState: Bundle? = null


    var kInteger: Int = 4

    var kFloat: Float = 4f

    var kDouble: Double = 41212.0

    var kLong: Long = 41212L

    var kString: String = "Sdfsdf"

    var kBoolean: Boolean = false

    val storePrcelable: SampleStore by lazy {
        val s = SampleStore()
        s.name = "아이"
        s.age = 1
        s
    }

    lateinit var storeMdfy: SampleStore

    init {
        val s = SampleStore()
        s.name = "아이빵"
        s.age = 4
        storeMdfy = s
    }

   private val kArrayListByStore: ArrayList<SampleStore> by lazy {
        arrayListOf(
                SampleStore(name = "아이놈", age = 4),
                SampleStore(name = "아이놈222", age = 5),
                SampleStore(name = "아이", age = 6)

        )
    }

    val store1 : SampleStore = SampleStore()

    fun save() {
        kArrayListByStore.add(SampleStore("asdfdsf", 3))
        kArrayListByStore.add(SampleStore("asdfds4444", 3))

        kArrayListByStore.add(SampleStore("asdfds4444", 4234))
        outState = Bundle()
        onSaveInstanceState(outState)
        Logger.d("FRAGMENT", "save > " + outState!!)
    }

    fun restore() {
        if (outState != null) {
            onCreate(outState)
        }
        Logger.d("FRAGMENT", "restore > " + outState!!)

        Logger.d("FRAGMENT", "restore > " + kArrayListByStore)
    }
}
