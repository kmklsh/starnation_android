package com.starnation.sample;

import android.os.Bundle;
import android.util.Log;

import com.starnation.android.Config;
import com.starnation.android.app.activity.AppCompatActivity;
import com.starnation.android.util.Logger;

public class SampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.d("sampleA", "onCreate activity savedInstanceState : " + savedInstanceState);

        if (savedInstanceState == null) {
            Config.setLogEnable(true);
            getSupportFragmentManager().beginTransaction().replace(R.id.activity_main, new SampleFragment()).commit();
        }
    }
}
