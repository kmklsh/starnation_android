package com.starnation.sample;

import com.starnation.android.Config;

/*
 * @author lsh
 * @since 2017. 8. 30.
*/
public class Application extends android.app.Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Config.setLogEnable(true);
    }
}
