package com.starnation.sample

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starnation.android.app.fragment.Fragment
import com.starnation.eventbus.Event
import kotlinx.android.synthetic.main.fragment_main.*

/*
 * @author lsh
 * @since 2017. 8. 30.
*/
class SampleFragment : Fragment<SampleViewModel, Event<*>>() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textview.setOnClickListener { v ->
            /*presenter.storePrcelable.age = 6
            presenter.storePrcelable.name = "아이 6"
            (v as TextView).text = presenter.storePrcelable.toString()*/
        }
        textview.setOnLongClickListener {
            startActivity(Intent(context, SampleActivity::class.java))
            false
        }

        button_save.setOnClickListener { viewmodel.save() }
        button_restore.setOnClickListener { viewmodel.restore() }

        button_viewmodel.setOnClickListener {
            startActivity(Intent(activity, SampleActivity::class.java))
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        /*(view!!.findViewById(R.id.textview) as TextView).text = presenter.storePrcelable.toString()*/
    }
}
