package com.starnation.sample.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/*
 * @author lsh
 * @since 2018. 3. 14.
*/
@Parcelize
data class KSampleParcel( var name: String? = null,
                          var age: Int = 0) : Parcelable {

    override fun toString(): String {
        return "name : $name age : $age"
    }
}
