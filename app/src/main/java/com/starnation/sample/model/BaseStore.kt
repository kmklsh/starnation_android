package com.starnation.sample.model

import android.os.Parcelable

/*
 * @author lsh
 * @since 2018. 3. 14.
*/
interface BaseStore : Parcelable
