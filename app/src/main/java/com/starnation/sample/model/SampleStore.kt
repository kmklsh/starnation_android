package com.starnation.sample.model

import android.annotation.SuppressLint
import kotlinx.android.parcel.Parcelize

/*
 * @author lsh
 * @since 2017. 8. 30.
*/
@SuppressLint("ParcelCreator")
@Parcelize
data class SampleStore(var name: String? = null, var age: Int = 0) : BaseStore {

    override fun toString(): String {
        return "$name $age"
    }
}
